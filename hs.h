// hs.h

#include "gsl/gsl_vector.h"
#include "gsl/gsl_matrix.h"

#include "global_definitions.h"

class base_parameters;
class muscle;
class CStopWatch;
class profiler;


class hs
{
	public:
		// Constructor and destructor
		hs::hs(int set_hs_number,double set_hsl,base_parameters* set_p_bp,muscle * set_p_m,
			double set_alpha_value, double set_beta_value);
		hs::~hs(void);

		// Variables
		int hs_number;				// an id for the half-sarcomere

		double initial_Ca;			// initial Ca2+ concentration in M

		double hsl;					// the length (nm) of the half_sarcomere
		
		double slack_length;		// the slack length of the half_sarcomere
		double command_length;		// the command length of the half_sarcomere

		double f_overlap;			// the proportion of sites that are in overlap
		double f_activated;			// the proportion of binding sites that are activated
		double f_bound;				// the proportion of sites that are bound
		double N;					// the proportion of sites that in overlap and
									// that are activated

		double df_inc;				// parameters that are associated with f_activated
		double df_dec;				// calculations
		double df_coop;
		double df_recruit;
		
		base_parameters * p_bp;		// a pointer to the base_parameters

		double cumulative_time;		// cumulative time from the beginning of
									// the simulation

		int no_of_x_bins;			// variables defining the cross-bridge
		double x_min;				// distributions
		double x_max;
		double x_increment;

		double sliding_velocity;	// sliding velocity of the filaments
		double last_velocity;

		double gaussian_width;

		//int no_of_cb_states;

		int n_array_length;

		muscle * p_parent_m;		// pointer to a parent muscle

		// GSL storeage
		gsl_vector * x;				// pointer to a gsl_vector holding
									// crossbridge bin positions

		gsl_matrix * n_matrix;		// pointer to a gsl_matrix holding
									// the populations, each column
									// holds a different isoform

/*		gsl_matrix * r_matrix [M_MAX_NO_OF_ISOFORMS];
									// pointer to a series of gsl_matrices,
									// each of which holds the rate
									// coefficients for a m_isoform

		gsl_matrix * a_matrix [M_MAX_NO_OF_ISOFORMS];
									// pointer to a series of gsl_matrices,
									// each of which is 1.0 for attachment rates
									// and zero otherwise
									// This is used for movement enhancement
*/

		gsl_matrix * cb_state_forces;
									// pointer to a gsl_matrix holding
									// cb state forces, each column
									// holds a different isoform

		gsl_matrix * cb_state_pops;
									// pointer to a gsl_matrix holding
									// cb state pops, each column
									// holds a different isoform
		
		// hs parameters
		double filament_compliance_factor;

		// Force structure
		struct forces_structure
		{
			double cb_force;
			double passive_force;
			double viscous_force;
			double total_force;
		};

		forces_structure estimated_forces;
		forces_structure actual_forces;

		profiler * p_profiler [NO_OF_HS_TIMERS];

		// alpha and beta - builds variability into forces
		double alpha_value;
		double beta_value;

		double hsl_temp;



		// Debugging
		int check_mode;

		// Member functions
//		void set_N(double time_step);
//		void set_f_overlap(void);
//		void set_f_activated(double time_step);
		double return_ld_factor(int i);
		double return_br_factor(int i, int j);
		double return_fd_factor(int i);
//		void set_f_bound(void);
//		void evolve_cb_populations(double time_step, int m_isoform);
		void evolve_kinetics(double time_step, int m_isoform);
		void shift_cb_distributions(double delta_hsl, int m_isoform);
		void calculate_forces_and_pops(double delta_hsl, 
			double sliding_velocity, int update_mode);
		void implement_time_step(double time_step,double delta_hsl,
			int evolve_only = 0, int no_evolve = 0);
//		void build_r_matrix(int i);
		double generic_rate(int rate_index,double x, int m_isoform);
		double return_cb_energy(int cb_state,double x, int m_isoform);
		double return_hs_length_for_force(double test_force,double time_step);
		double sum_of_gsl_vector(gsl_vector * gsl_v);
		double return_n_bound(const double * y);
		double return_f_overlap(double x);

		// Debugging functions
		void check_int(int);
		double sum_of_n_matrix(void);
		void dump_rate_constants(void);
		void dump_force_dependence(void);
		void dump_energies(void);
		void dump_f_overlap_function_to_file(void);
		void dump_populations_to_file(int append);
		void dump_status_to_file(int append);
		void dump_gsl_matrix(gsl_matrix * m, char * output_file_string);
};
