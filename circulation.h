// Circulation.h

#include "gsl/gsl_vector.h"
#include "gsl/gsl_matrix.h"

#include "global_definitions.h"

class base_parameters;
class muscle;
class CStopWatch;
class profiler;


class circulation
{
	public:
		// Constructor and destructor
		circulation::circulation(base_parameters* set_p_bp,
			int set_condition_number, int set_thread_number);
		circulation::~circulation(void);

		// Variables

		// Pointers
		base_parameters * p_bp;
		muscle * p_m;

		// Single vlaues
		int condition_number;
		int thread_number;

		double lv_volume;
		double lv_pressure;

		double circulation_slack_hsl;
		double slack_lv_circumference;
		double current_lv_circumference;

		// Vectors
		gsl_vector * compartment_volumes;
		gsl_vector * compartment_pressures;

		gsl_vector * r_values;
		gsl_vector * c_values;

		// Functions
		double return_ventricular_pressure(double lv_volume);
		double return_ventricular_circumference(double lv_volume);
};
