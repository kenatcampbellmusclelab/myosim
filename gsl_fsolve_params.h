// gsl_fsolve_params
class muscle;

class gsl_fsolve_params
{
public:
	gsl_fsolve_params::gsl_fsolve_params();
	gsl_fsolve_params:: ~gsl_fsolve_params();

	// Variables
	muscle * p_muscle;
	int length_control;

	double clamp_force;
};
