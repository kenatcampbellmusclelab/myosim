// muscle.h

class hs;
class base_parameters;
class gsl_fsolve_params;

#include "gsl/gsl_vector.h"
#include "gsl/gsl_matrix.h"

#include "global_definitions.h"

class muscle
{
public:
	muscle::muscle(base_parameters * set_p_bp, int set_condition_number, int set_thread_number);
	muscle::~muscle(void);

	// Variables

	base_parameters * p_bp;

	int thread_number;
	int condition_number;
	int t_counter;

	int isotonic_mode;				// 1 is normal
									// 0 is no control
	int isotonic_flag;

	double time_step;

	hs * p_hs [MAX_NO_OF_HALF_SARCOMERES];

	muscle * p_self_muscle;

	gsl_fsolve_params * p_fsolve_params;
									// This is a parameter that is used to
									// solve simulations with mulitple half-sarcomeres

	double current_pCa;
	double current_Pi_concentration;
	double current_ADP_concentration;
	double current_ATP_concentration;

	double isotonic_force;

	double current_force;
	double current_length;

	double original_length;
	double min_length;
	double series_extension;

	double m_series_k;				// stiffness of series spring
	double m_series_alpha;
	double m_series_beta;

	double m_k_plus;
	double m_k_minus;

	double last_length;

	double windkessel_force;

	// Functions
	void step_simulation(double time_step,double delta_hsl);
	void step_single_half_sarcomere(double time_step,double delta_hsl);
	void step_multiple_half_sarcomeres(double time_step,double delta_hsl);
	void dump_populations_to_file(void);

	double return_series_stretch(double f);
	void test_series_stretch(void);

	void initialize_gsl_length_control_vector(gsl_vector * x, double delta_hsl);
	void initialize_gsl_tension_control_vector(gsl_vector * x);
	static int gsl_match_hs_forces_wrapper(const gsl_vector *x, void *params, gsl_vector *f);
	void gsl_match_hs_forces_length_control(const gsl_vector * x, gsl_vector * f);
	void gsl_match_hs_forces_tension_control(const gsl_vector * x, gsl_vector * f);
	void gsl_length_control_wrap_up(gsl_vector * x);
	void gsl_tension_control_wrap_up(gsl_vector * x);
};