// Myosim.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "math.h"

#include "muscle.h"
#include "hs.h"
#include "base_parameters.h"
#include "hr_timer.h"
#include "data_holder.h"
#include "global_definitions.h"
#include "profiler.h"
#include "circulation.h"

#include "gsl/gsl_vector.h"
#include "gsl/gsl_matrix.h"
#include "gsl/gsl_math.h"

// Variables

int i,j;						// looping variables
char temp_string[MAX_STRING_LENGTH];
								// a temporary string

char instruction_file_string[MAX_STRING_LENGTH];
char output_directory_string[MAX_STRING_LENGTH];

base_parameters * p_bp;			// pointer to a base_parameters object
data_holder * p_data_holder;	// pointer to a data_holder object
CStopWatch main_stopwatch;		// a new stopwatch object

circulation * p_c;				// pointer to a circulation object

gsl_matrix * profile_times;
gsl_matrix * profile_calls;
double total_profiled_time;

// Functions
void run_conditions_as_threads(void);
void create_result_file(void);
void create_summary_file(void);
void create_population_file(void);
void test_run(void);

void run_circulation_experiments(void);
void run_circulations_as_threads(void);

void dump_temp_file(void);

// Data to do with multi-threading
struct simulation_data
{
	data_holder * p_data_holder;
	int condition_number;
	int thread_number;
};
simulation_data * p_simulation_data [MAX_NO_OF_CONDITIONS];
DWORD WINAPI run_condition_thread(LPVOID lpParam);
DWORD WINAPI run_circulation_thread(LPVOID lpParam);

// Main function

int _tmain(int argc, _TCHAR* argv[])
{
	// Variables
	char working_directory_string[MAX_STRING_LENGTH];
	
	// Start the timer
	main_stopwatch.startTimer();

	// Set defaults
	sprintf_s(instruction_file_string,MAX_STRING_LENGTH,"%s",
		DEFAULT_INSTRUCTION_FILE_STRING);

	// Generate the default output directory

	// Windows specific
	GetCurrentDirectory(MAX_STRING_LENGTH,working_directory_string);
	char * p_char;
	p_char = strrchr(working_directory_string,'\\');
	strncpy(output_directory_string,working_directory_string,
		p_char-working_directory_string+1);
	sprintf(output_directory_string,"%s\\%s",
		output_directory_string,DEFAULT_OUTPUT_DIRECTORY_STRING);
	printf("%s\n",output_directory_string);

	// Write any input arguments to command_line_string;
	if (argc>1)
	{
		sprintf_s(instruction_file_string,MAX_STRING_LENGTH,
			"%s",argv[1]);
	}

	if (argc>=2)
	{
		sprintf_s(output_directory_string,MAX_STRING_LENGTH,
			"%s",argv[2]);
	}

	// Load the base_parameters from the instruction file
	p_bp = new base_parameters(instruction_file_string,
		output_directory_string);

	// Branch depending on experiment mode
	switch (p_bp->circulation_mode)
	{
		case 1:
			// Circulation mode

			// Run a protocol specified in the instruction file
			p_bp->display_base_parameters();
			p_bp->dump_base_parameters();

			printf("Circulation mode\n");

			run_circulation_experiments();

			break;

		case -1:
			// Test run
			test_run();
			break;

		case 0:
			// Run a protocol specified in the instruction file
			p_bp->display_base_parameters();
			p_bp->dump_base_parameters();

			// Initialise arrays for profile times
			profile_times = gsl_matrix_alloc(p_bp->no_of_conditions,NO_OF_HS_TIMERS);
			profile_calls = gsl_matrix_alloc(p_bp->no_of_conditions,NO_OF_HS_TIMERS);

			// Create a data holder
			p_data_holder = new data_holder(p_bp);

			// Create muscles for each condition
			run_conditions_as_threads();

			// And supporting data
			create_summary_file();
			create_population_file();

	//		dump_temp_file();

			// Profile information
			total_profiled_time=0.0;
			for (i=0;i<p_bp->no_of_conditions;i++)
			{
				for (j=0;j<NO_OF_HS_TIMERS;j++)
				{
					printf("Condition[%i] Stopwatch[%i] %g Calls %i\n",
						i,j,gsl_matrix_get(profile_times,i,j),
						(int)gsl_matrix_get(profile_calls,i,j));
					total_profiled_time = total_profiled_time +
						gsl_matrix_get(profile_times,i,j);
				}
			}

			// Stop the timer and display
			main_stopwatch.stopTimer();
			printf("Main function took: %g s\n",main_stopwatch.getElapsedTime());
			printf("Profiled time was: %g s\n",total_profiled_time);

			break;

		default:
			printf("Unknown circulation mode\n");
			printf("Now finishing\n");
			break;
	}

	// Tidy up
	delete p_data_holder;
	delete p_bp;

	return 0;
}

// Functions

void run_circulation_experiments(void)
{
	// Run circulation experiments

	p_bp->display_base_parameters();
	p_bp->dump_base_parameters();

	// Initialise arrays for profile times
	profile_times = gsl_matrix_alloc(p_bp->no_of_conditions,NO_OF_HS_TIMERS);
	profile_calls = gsl_matrix_alloc(p_bp->no_of_conditions,NO_OF_HS_TIMERS);

	// Create a data holder
	p_data_holder = new data_holder(p_bp);

	// Run circulations as threads
	run_circulations_as_threads();

	// And supporting data
	create_summary_file();
	create_population_file();

	// Profile information
	total_profiled_time=0.0;
	for (i=0;i<p_bp->no_of_conditions;i++)
	{
		for (j=0;j<NO_OF_HS_TIMERS;j++)
		{
			printf("Condition[%i] Stopwatch[%i] %g Calls %i\n",
				i,j,gsl_matrix_get(profile_times,i,j),
				(int)gsl_matrix_get(profile_calls,i,j));
			total_profiled_time = total_profiled_time +
				gsl_matrix_get(profile_times,i,j);
		}
	}

	// Stop the timer and display
	main_stopwatch.stopTimer();
	printf("Main function took: %g s\n",main_stopwatch.getElapsedTime());
	printf("Profiled time was: %g s\n",total_profiled_time);
}

void run_circulations_as_threads(void)
{
	// Code launches circulations as separate threads
	// This allows a PC to simulate different experiments in parallel

	// Variables
	int thread_counter;

	// Create space for the threads
	DWORD * dwThreadID = new DWORD [p_bp->no_of_conditions];
	HANDLE * hThread = new HANDLE [p_bp->no_of_conditions];

	// Launch threads
	for (thread_counter=0;thread_counter<p_bp->no_of_conditions;thread_counter++)
	{
		printf("Launching thread %i\n",thread_counter);

		p_simulation_data[thread_counter] = new simulation_data();
		p_simulation_data[thread_counter]->p_data_holder = p_data_holder;
		p_simulation_data[thread_counter]->condition_number = thread_counter;
		p_simulation_data[thread_counter]->thread_number = thread_counter;

		// Launch it
		hThread[thread_counter] = CreateThread(
			NULL,0,run_circulation_thread,p_simulation_data[thread_counter],0,
			&dwThreadID[thread_counter]);

		// Check it started
		if (hThread[thread_counter]==NULL)
		{
			printf("Thread problem in run_circulations_as_threads.\nNow exiting\n");
			ExitProcess(thread_counter);
			exit(1);
		}
	}

	// Wait until all threads have completed
	WaitForMultipleObjects(p_bp->no_of_conditions,hThread,TRUE,INFINITE);

	// Tidy up
	for (thread_counter=0;thread_counter<=p_bp->no_of_conditions;thread_counter++)
	{
		CloseHandle(hThread[thread_counter]);
	}

	delete [] dwThreadID;
	delete [] hThread;
}

DWORD WINAPI run_circulation_thread(LPVOID lpParam)
{
	// This function runs a single circulation as a thread

	// Variables
	int i;
	int t_counter;
	int thread_number;
	int condition_number;

	double time_step;
	
	circulation * p_c;

	data_holder * p_data_holder;

	// Code
	condition_number = ((simulation_data *)lpParam)->condition_number;
	thread_number = ((simulation_data *)lpParam)->thread_number;
	p_data_holder = ((simulation_data *)lpParam)->p_data_holder;

	// Display
	printf("Starting run_circulation_thread, thread_number: %i condition_number: %i\n",
		thread_number,condition_number);

	// Create a circulation
	p_c = new circulation(p_bp,condition_number,thread_number);

	for (t_counter=0;t_counter<p_bp->no_of_time_points;t_counter++)
	{
		p_c->p_m->current_pCa = gsl_matrix_get(p_bp->pCa_values,t_counter,condition_number);
		p_c->p_m->current_Pi_concentration =
			gsl_matrix_get(p_bp->Pi_concentrations,t_counter,condition_number);
		p_c->p_m->current_ADP_concentration =
			gsl_matrix_get(p_bp->ADP_concentrations,t_counter,condition_number);
		p_c->p_m->current_ATP_concentration =
			gsl_matrix_get(p_bp->ATP_concentrations,t_counter,condition_number);

		// Step the simulation
		p_c->p_m->t_counter = t_counter;

		time_step = gsl_vector_get(p_bp->time_steps,t_counter);
		p_c->p_m->p_hs[0]->evolve_kinetics(time_step,0);

		p_c->

		p_c->lv_pressure = p_c->return_ventricular_pressure(
							p_c->lv_volume);

		// Store data
		gsl_matrix_set(p_data_holder->simulation_forces,
			t_counter,condition_number,
			p_c->p_m->current_force);
		gsl_matrix_set(p_data_holder->cb_forces,
			t_counter,condition_number,
			p_c->p_m->p_hs[0]->actual_forces.cb_force);
		gsl_matrix_set(p_data_holder->passive_forces,
			t_counter,condition_number,
			p_c->p_m->p_hs[0]->actual_forces.passive_force);
		gsl_matrix_set(p_data_holder->viscous_forces,
			t_counter,condition_number,
			p_c->p_m->p_hs[0]->actual_forces.viscous_force);
		gsl_matrix_set(p_data_holder->hs_lengths,
			t_counter,condition_number,
			p_c->p_m->p_hs[0]->hsl);
		gsl_matrix_set(p_data_holder->command_lengths,
			t_counter,condition_number,
			p_c->p_m->p_hs[0]->command_length);
		gsl_matrix_set(p_data_holder->slack_lengths,
			t_counter,condition_number,
			p_c->p_m->p_hs[0]->slack_length);
		gsl_matrix_set(p_data_holder->f_activated,
			t_counter,condition_number,
			p_c->p_m->p_hs[0]->f_activated);
		gsl_matrix_set(p_data_holder->f_overlap,
			t_counter,condition_number,
			p_c->p_m->p_hs[0]->f_overlap);
		gsl_matrix_set(p_data_holder->f_bound,
			t_counter,condition_number,
			p_c->p_m->p_hs[0]->f_bound);
		gsl_matrix_set(p_data_holder->series_extension,
			t_counter,condition_number,
			p_c->p_m->series_extension);

		for (i=0;i<p_bp->circulation_no_of_compartments;i++)
		{
			gsl_matrix_set(p_data_holder->circulation_volumes[condition_number],
				t_counter,i,
				gsl_vector_get(p_c->compartment_volumes,i));

			gsl_matrix_set(p_data_holder->circulation_pressures[condition_number],
				t_counter,i,
				gsl_vector_get(p_c->compartment_pressures,i));
		}

		// Display to show we are active
		if (((t_counter%50)==0)||(t_counter==0))
		{
			printf("t=%i\tLV volume %g, LV pressure: %g\n",
				t_counter,p_c->lv_volume,p_c->lv_pressure);
		}
	}

	// Hold the timing information
	for (i=0;i<NO_OF_HS_TIMERS;i++)
	{
		gsl_matrix_set(profile_times,condition_number,i,
			p_c->p_m->p_hs[0]->p_profiler[i]->timer);
		gsl_matrix_set(profile_calls,condition_number,i,
			(double)p_c->p_m->p_hs[0]->p_profiler[i]->function_calls);
	}

	// Display
	printf("Finishing run_circulation_thread, thread_number: %i condition_number %i\n",
		thread_number,condition_number);

	// Tidy up
	delete p_c;

	// Return
	return 1;
}

void test_run(void)
{
	// Variables
	int i;
	int n=100;

	double time_step = 0.01;	// seconds
	double delta_hsl = 0.0;		// units in nm


	muscle * p_m;			// a pointer to a muscle object

	// Code

	// First create a muscle

	// p_m is a pointer to a muscle that is defined by the base_parameters
	p_m = new muscle(p_bp,0,0);
	p_m->isotonic_force = -2.0;	// use this to keep control simple

	// Set the activation level by pCa
	// In these units, 9.0 is off, 4.5 is max
	p_m->current_pCa = 9.0;

	// Go through a loop
	i=1;
	while (i<=n)
	{
		// Report the activation level, length and force
		printf("Counter %i\tpCa %g\tLength %g\tForce %g\n",
			i,p_m->current_pCa,p_m->current_length,p_m->current_force);
		printf("Bound cbs %g\n",p_m->p_hs[0]->f_bound);

		// Step the activation
		if (i>=30)
			p_m->current_pCa=4.5;

		if (i>50)
			delta_hsl = 0.5;

		// Implement the time_step
		p_m->step_simulation(time_step,delta_hsl);

		// Increment the counter
		i++;
	}

	// Tidy up
	delete p_m;
}


void run_conditions_as_threads(void)
{
	// Code launches condition simulations as separate threads
	// This allows the PC to simulate different experiments in parallel

	// Variables
	int thread_counter;

	// Create space for the threads
	DWORD * dwThreadID = new DWORD [p_bp->no_of_conditions];
	HANDLE * hThread = new HANDLE [p_bp->no_of_conditions];

	// Launch threads
	for (thread_counter=0;thread_counter<p_bp->no_of_conditions;thread_counter++)
	{
		p_simulation_data[thread_counter] = new simulation_data();
		p_simulation_data[thread_counter]->p_data_holder = p_data_holder;
		p_simulation_data[thread_counter]->condition_number=thread_counter;
		p_simulation_data[thread_counter]->thread_number=thread_counter;

		// Launch it
		hThread[thread_counter] = CreateThread(
			NULL,0,run_condition_thread,p_simulation_data[thread_counter],0,
			&dwThreadID[thread_counter]);

		// Check it started
		if (hThread[thread_counter]==NULL)
		{
			printf("Thread problem in run_conditions_as_threads.\nNow exiting\n");
			ExitProcess(thread_counter);
			exit(1);
		}
	}

	// Wait until all threads have completed
	WaitForMultipleObjects(p_bp->no_of_conditions,hThread,TRUE,INFINITE);

	if (p_bp->no_of_conditions>50)
	{
		printf("\aSleeping\n");
		Sleep(1000);
	}

	// Tidy up
	for (thread_counter=0;thread_counter<p_bp->no_of_conditions;thread_counter++)
	{
		CloseHandle(hThread[thread_counter]);
	}

	delete [] dwThreadID;
	delete [] hThread;
}

DWORD WINAPI run_condition_thread(LPVOID lpParam)
{
	// This function runs a single condition as a thread

	// Variables
	int i,j;
	int t_counter;
	int thread_number;
	int hs_counter;
	int condition_number;
	int col_index;

	double length_holder;

	muscle * p_m;

	data_holder * p_data_holder;

	// Code
	condition_number = ((simulation_data *)lpParam)->condition_number;
	thread_number = ((simulation_data *)lpParam)->thread_number;
	p_data_holder = ((simulation_data *)lpParam)->p_data_holder;

	// Display
	printf("Starting run_condition_thread, thread_number: %i condition_number %i\n",
		thread_number,condition_number);

	// Create a muscle
	p_m = new muscle(p_bp,condition_number,thread_number);
	
	// Dump the f_overlap function
	if ((condition_number==0)&&(p_bp->debug_dump_mode!=0))
		p_m->p_hs[0]->dump_f_overlap_function_to_file();

	// Cycle through the time_points
	for (t_counter=0;t_counter<p_bp->no_of_time_points;t_counter++)
	{
		// Assign values for the muscle
		p_m->current_pCa = gsl_matrix_get(p_bp->pCa_values,t_counter,condition_number);
		p_m->current_Pi_concentration = 
			gsl_matrix_get(p_bp->Pi_concentrations,t_counter,condition_number);
		p_m->current_ADP_concentration = 
			gsl_matrix_get(p_bp->ADP_concentrations,t_counter,condition_number);
		p_m->current_ATP_concentration = 
			gsl_matrix_get(p_bp->ATP_concentrations,t_counter,condition_number);
		p_m->isotonic_force =
			gsl_matrix_get(p_bp->isotonic_forces,t_counter,condition_number);

		// Step the simulation
		p_m->t_counter=t_counter;
		p_m->step_simulation(gsl_vector_get(p_bp->time_steps,t_counter),
			gsl_matrix_get(p_bp->hs_increments,t_counter,condition_number));

		// Update the data arrays

		length_holder=0.0;

		// Cycle through the half-sarcomeres
		for (hs_counter=0;hs_counter<p_bp->no_of_half_sarcomeres;hs_counter++)
		{
			col_index=((condition_number*p_bp->no_of_half_sarcomeres)+hs_counter);
			
			gsl_matrix_set(p_data_holder->simulation_forces,t_counter,col_index,
				p_m->current_force);
			gsl_matrix_set(p_data_holder->cb_forces,t_counter,col_index,
				p_m->p_hs[hs_counter]->actual_forces.cb_force);
			gsl_matrix_set(p_data_holder->passive_forces,t_counter,col_index,
				p_m->p_hs[hs_counter]->actual_forces.passive_force);
			gsl_matrix_set(p_data_holder->viscous_forces,t_counter,col_index,
				p_m->p_hs[hs_counter]->actual_forces.viscous_force);
			gsl_matrix_set(p_data_holder->hs_lengths,t_counter,col_index,
				p_m->p_hs[hs_counter]->hsl);
			gsl_matrix_set(p_data_holder->command_lengths,t_counter,col_index,
				p_m->p_hs[hs_counter]->command_length);
			gsl_matrix_set(p_data_holder->slack_lengths,t_counter,col_index,
				p_m->p_hs[hs_counter]->slack_length);
			gsl_matrix_set(p_data_holder->f_activated,t_counter,col_index,
				p_m->p_hs[hs_counter]->f_activated);
			gsl_matrix_set(p_data_holder->f_overlap,t_counter,col_index,
				p_m->p_hs[hs_counter]->f_overlap);
			gsl_matrix_set(p_data_holder->f_bound,t_counter,col_index,
				p_m->p_hs[hs_counter]->f_bound);
			gsl_matrix_set(p_data_holder->series_extension,t_counter,col_index,
				p_m->series_extension);

//gsl_matrix_set(p_data_holder->temp,t_counter,col_index,p_m->p_hs[hs_counter]->hsl_temp);

			if (hs_counter<(p_bp->no_of_half_sarcomeres_in_series))
			{
				length_holder = length_holder + p_m->p_hs[hs_counter]->hsl;
			}

		}

			/*
			// Store data about df_activated/dt
			gsl_matrix_set(p_data_holder->df_matrix,t_counter,
					(condition_number*HS_DF_ACTIVATED_TERMS),
				p_m->p_hs[0]->df_inc);
			gsl_matrix_set(p_data_holder->df_matrix,t_counter,
					(condition_number*HS_DF_ACTIVATED_TERMS)+1,
				p_m->p_hs[0]->df_dec);
			gsl_matrix_set(p_data_holder->df_matrix,t_counter,
					(condition_number*HS_DF_ACTIVATED_TERMS)+2,
				p_m->p_hs[0]->df_coop);
			*/

		for (hs_counter=0;hs_counter<p_bp->no_of_half_sarcomeres;hs_counter++)
		{
			// Store data about cb forces and populations
			for (i=0;i<p_bp->m_no_of_isoforms;i++)
			{
				for (j=0;j<p_bp->no_of_states;j++)
				{
					col_index = (condition_number * 
						(p_bp->no_of_half_sarcomeres*p_bp->m_no_of_isoforms*p_bp->no_of_states)) +
						(i*p_bp->no_of_states) + j;

					gsl_matrix_set(p_data_holder->state_forces[i],
							t_counter,col_index,
						gsl_matrix_get(p_m->p_hs[hs_counter]->cb_state_forces,j,i));

					gsl_matrix_set(p_data_holder->state_pops[i],
							t_counter,col_index,
						gsl_matrix_get(p_m->p_hs[hs_counter]->cb_state_pops,j,i));
				}
			}
		}

		for (hs_counter=0;hs_counter<p_bp->no_of_half_sarcomeres;hs_counter++)
		{
			col_index=((condition_number*p_bp->no_of_half_sarcomeres)+hs_counter);

			gsl_matrix_set(p_data_holder->muscle_length,t_counter,col_index,
				p_m->series_extension + length_holder);
		}

		// Display to show we are active
		if (((t_counter%50)==0)||(t_counter==0))
		{
			printf("t=%i\tMuscle[%i], force: %g\n",
				t_counter,condition_number,p_m->current_force);
		}
	}

	// Hold the timing information
	for (i=0;i<NO_OF_HS_TIMERS;i++)
	{
		gsl_matrix_set(profile_times,condition_number,i,
			p_m->p_hs[0]->p_profiler[i]->timer);
		gsl_matrix_set(profile_calls,condition_number,i,
			(double)p_m->p_hs[0]->p_profiler[i]->function_calls);
	}

	// Display
	printf("Finishing run_condition_thread, thread_number: %i condition_number %i\n",
		thread_number,condition_number);

	// Tidy up
	delete p_m;

	// Return
	return 1;
}

void create_result_file(void)
{
	// Output results file

	// Variables
	int i,j;
	char result_file_string [MAX_STRING_LENGTH];

	// Open the results file
	FILE * result_file;
	sprintf_s(result_file_string,MAX_STRING_LENGTH,"%s\\%i_result.txt",
		"output",p_bp->particle_number);
	fopen_s(&result_file,result_file_string,"w");

	if (result_file==NULL)
	{
		printf("create_result_file: %s could not be opened\n",result_file_string);
		printf("Now exiting\n");
		exit(1);
	}

	// Output
	fprintf(result_file,"%i\n",p_bp->particle_number);
	fprintf(result_file,"%i\n\n",p_bp->check_number);

	fprintf(result_file,"MACHINE_NAME\nIP_ADDRESS\n\n");

	fprintf(result_file,"Execution_time: %g s\n\n",(double)main_stopwatch.getElapsedTime());
	fprintf(result_file,"Repeats completed: 1\n\n");

	// Data
	fprintf(result_file,"Data\n");
	for (i=0;i<p_bp->no_of_time_points;i++)
	{
		for (j=0;j<p_bp->no_of_conditions;j++)
		{
			if (p_bp->error_flag==1)
			{
				fprintf(result_file,"0.0");
			}
			else
			{
				fprintf(result_file,"%g",
					gsl_matrix_get(p_data_holder->simulation_forces,i,j));
			}

			if (j==(p_bp->no_of_conditions-1))
				fprintf(result_file,"\n");
			else
				fprintf(result_file,"\t");
		}
	}
	fclose(result_file);
}

void dump_temp_file(void)
{
/*	int i,j;
	FILE * temp_file;
	char temp_file_string [MAX_STRING_LENGTH];
	sprintf_s(temp_file_string,MAX_STRING_LENGTH,"%s\\temp.txt",
			output_directory_string);
	fopen_s(&temp_file,temp_file_string,"w");
	
	for (j=0;j<p_bp->no_of_time_points;j++)
	{
		fprintf(temp_file,"%g\n",gsl_matrix_get(p_data_holder->temp,j,0));
	}
	fclose(temp_file);
*/
}


void create_summary_file(void)
{
	// Output summary data

	// Variables
	int i,j,k,m;
	int col_index;

	double time_value;
	char summary_file_string [MAX_STRING_LENGTH];

	// Code

	// Create the summary file
	FILE * summary_file;

	sprintf_s(summary_file_string,MAX_STRING_LENGTH,"%s\\summary.txt",
			output_directory_string);

	fopen_s(&summary_file,summary_file_string,"w");

	if (summary_file==NULL)
	{
		printf("create_summary_file: %s could not be opened\n",summary_file_string);
		printf("Now exiting\n");
		exit(1);
	}

	// Output data
	fprintf(summary_file,"Myosim summary output\n");
	fprintf(summary_file,"Header lines 7\n");
	fprintf(summary_file,"Number of half-sarcomeres %i\n",p_bp->no_of_half_sarcomeres);
	fprintf(summary_file,"Circulation mode %i\n",p_bp->circulation_mode);
	fprintf(summary_file,"Number of circulatory compartments %i\n",p_bp->circulation_no_of_compartments);
	fprintf(summary_file,"Myosim version %f\n",MYOSIM_VERSION);

	fprintf(summary_file,"time_s\t");
	for (j=0;j<p_bp->no_of_conditions;j++)
	{
		for (k=0;k<p_bp->no_of_half_sarcomeres;k++)
		{
			fprintf(summary_file,"force_%i_%i\t",j+1,k+1);
			fprintf(summary_file,"cb_force_%i_%i\t",j+1,k+1);
			fprintf(summary_file,"pas_force_%i_%i\t",j+1,k+1);
			fprintf(summary_file,"viscous_force_%i_%i\t",j+1,k+1);
			fprintf(summary_file,"hs_length_%i_%i\t",j+1,k+1);
			fprintf(summary_file,"command_length_%i_%i\t",j+1,k+1);
			fprintf(summary_file,"slack_length_%i_%i\t",j+1,k+1);
			fprintf(summary_file,"f_activated_%i_%i\t",j+1,k+1);
			fprintf(summary_file,"f_bound_%i_%i\t",j+1,k+1);
			fprintf(summary_file,"f_overlap_%i_%i\t",j+1,k+1);
			fprintf(summary_file,"pCa_%i_%i\t",j+1,k+1);
			fprintf(summary_file,"Ca_%i_%i\t",j+1,k+1);
			fprintf(summary_file,"series_extension_%i_%i\t",j+1,k+1);
			fprintf(summary_file,"muscle_length_%i_%i",j+1,k+1);

			if (p_bp->circulation_mode==0)
			{
				if ((j==(p_bp->no_of_conditions-1))&&(k==(p_bp->no_of_half_sarcomeres-1)))
					fprintf(summary_file,"\n");
				else
					fprintf(summary_file,"\t");
			}
			else
			{
				fprintf(summary_file,"\t");
	
				for (m=0;m<p_bp->circulation_no_of_compartments;m++)
				{
					fprintf(summary_file,"volume_%i_%i_%i\t",j+1,k+1,m+1);
					fprintf(summary_file,"pressure_%i_%i_%i",j+1,k+1,m+1);

					if ((j==(p_bp->no_of_conditions-1))&&
						(k==(p_bp->no_of_half_sarcomeres-1))&&
							(m==(p_bp->circulation_no_of_compartments-1)))
					{
						fprintf(summary_file,"\n");
					}
					else
					{
						fprintf(summary_file,"\t");
					}
				}
			}
		}
	}

	time_value=0.0;
	for (i=0;i<p_bp->no_of_time_points;i++)
	{
		time_value = time_value + gsl_vector_get(p_bp->time_steps,i);
		fprintf(summary_file,"%.5f\t",time_value);

		for (j=0;j<p_bp->no_of_conditions;j++)
		{
			for (k=0;k<p_bp->no_of_half_sarcomeres;k++)
			{
				col_index=((j*p_bp->no_of_half_sarcomeres)+k);

				if (p_bp->error_flag==0)
				{
					fprintf(summary_file,"%g\t",GSL_MAX(
						gsl_matrix_get(p_data_holder->simulation_forces,i,col_index),0.0));
				}
				else
				{
					fprintf(summary_file,"0.0\t");
				}

				fprintf(summary_file,"%g\t",
					gsl_matrix_get(p_data_holder->cb_forces,i,col_index));
				fprintf(summary_file,"%g\t",
					gsl_matrix_get(p_data_holder->passive_forces,i,col_index));
				fprintf(summary_file,"%g\t",
					gsl_matrix_get(p_data_holder->viscous_forces,i,col_index));

				if (p_bp->error_flag==0)
				{
					fprintf(summary_file,"%.4f\t",
						gsl_matrix_get(p_data_holder->hs_lengths,i,col_index));
				}
				else
				{
					fprintf(summary_file,"0.0\t");
				}
				fprintf(summary_file,"%g\t",
					gsl_matrix_get(p_data_holder->command_lengths,i,col_index));
				fprintf(summary_file,"%g\t",
					gsl_matrix_get(p_data_holder->slack_lengths,i,col_index));
				fprintf(summary_file,"%g\t",
					gsl_matrix_get(p_data_holder->f_activated,i,col_index));
				fprintf(summary_file,"%g\t",
					gsl_matrix_get(p_data_holder->f_bound,i,col_index));
				fprintf(summary_file,"%g\t",
					gsl_matrix_get(p_data_holder->f_overlap,i,col_index));
				fprintf(summary_file,"%g\t",
					gsl_matrix_get(p_bp->pCa_values,i,j));
				fprintf(summary_file,"%g\t",
					pow(10,-gsl_matrix_get(p_bp->pCa_values,i,j)));
				fprintf(summary_file,"%g\t",
					gsl_matrix_get(p_data_holder->series_extension,i,col_index));
				fprintf(summary_file,"%g",
					gsl_matrix_get(p_data_holder->muscle_length,i,col_index));

				if (p_bp->circulation_mode==0)
				{
					if ((j==(p_bp->no_of_conditions-1))&&(k==(p_bp->no_of_half_sarcomeres-1)))
						fprintf(summary_file,"\n");
					else
						fprintf(summary_file,"\t");
				}
				else
				{
					fprintf(summary_file,"\t");

					for (m=0;m<p_bp->circulation_no_of_compartments;m++)
					{
						fprintf(summary_file,"%g\t",
							gsl_matrix_get(p_data_holder->circulation_volumes[j],i,m));
						fprintf(summary_file,"%g",
							gsl_matrix_get(p_data_holder->circulation_pressures[j],i,m));

						if ((j==(p_bp->no_of_conditions-1)) &&
							(k==(p_bp->no_of_half_sarcomeres-1)) &&
							(m==(p_bp->circulation_no_of_compartments-1)))
						{
							fprintf(summary_file,"\n");
						}
						else
						{
							fprintf(summary_file,"\t");
						}
					}
				}

			}
		}
	}

	printf("Summary file successfully written:\n %s\n",summary_file_string);

	fclose(summary_file);
}

void create_population_file(void)
{
	// Output cross-bridge populations

	// Variables
	int h_counter,i,j,k;
	int m_counter;
	double time_value;
	char population_file_string [MAX_STRING_LENGTH];

	// Code

	// Create the population file
	FILE * population_file;

	sprintf_s(population_file_string,MAX_STRING_LENGTH,"%s\\populations.txt",
			output_directory_string);

	fopen_s(&population_file,population_file_string,"w");

	if (population_file==NULL)
	{
		printf("create_population_file: %s could not be opened\n",population_file_string);
		printf("Now exiting\n");
		exit(1);
	}

	// Output data
	fprintf(population_file,"Population output\n");
	fprintf(population_file,"No of half-sarcomeres\n",p_bp->no_of_half_sarcomeres);
	fprintf(population_file,"no_of_m_isoforms: %i\n",p_bp->m_no_of_isoforms);
	fprintf(population_file,"no_of_conditions: %i\n",p_bp->no_of_conditions);
	// Write header
	fprintf(population_file,"Time_s\t");

	for (h_counter=0;h_counter<p_bp->no_of_half_sarcomeres;h_counter++)
	{
		for (m_counter=0;m_counter<p_bp->m_no_of_isoforms;m_counter++)
		{
			for (i=0;i<p_bp->no_of_conditions;i++)
			{
				for (j=0;j<p_bp->no_of_states;j++)
				{
					fprintf(population_file,"HS_%i_M%i_C%i_State%i",h_counter+1,m_counter+1,i+1,j+1);

					if ((h_counter==(p_bp->no_of_half_sarcomeres-1)) &&
						(m_counter==(p_bp->m_no_of_isoforms-1)) &&
						(i==(p_bp->no_of_conditions-1)) &&
						(j==(p_bp->no_of_states-1)))
					{
						fprintf(population_file,"\n");
					}
					else
					{
						fprintf(population_file,"\t");
					}
				}
			}
		}
	}

	// Output data
	time_value=0.0;
	for (k=0;k<p_bp->no_of_time_points;k++)
	{
		time_value=time_value+gsl_vector_get(p_bp->time_steps,k);
		fprintf(population_file,"%g\t",time_value);

		for (h_counter=0;h_counter<p_bp->no_of_half_sarcomeres;h_counter++)
		{
			for (m_counter=0;m_counter<p_bp->m_no_of_isoforms;m_counter++)
			{
				for (i=0;i<p_bp->no_of_conditions;i++)
				{
					for (j=0;j<p_bp->no_of_states;j++)
					{

						fprintf(population_file,"%g",
							gsl_matrix_get(p_data_holder->state_pops[m_counter],
								k,(i*p_bp->no_of_states)+j));

						if ((h_counter==(p_bp->no_of_half_sarcomeres-1)) &&
							(m_counter==(p_bp->m_no_of_isoforms-1)) &&
							(i==(p_bp->no_of_conditions-1)) &&
							(j==(p_bp->no_of_states-1)))
						{
							fprintf(population_file,"\n");
						}
						else
						{
							fprintf(population_file,"\t");
						}
					}
				}
			}
		}
	}
	
	// Tidy up
	fclose(population_file);
}