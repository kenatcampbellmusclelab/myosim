// data_holder.h

#include "gsl/gsl_vector.h"
#include "gsl/gsl_matrix.h"
#include "global_definitions.h"

class base_parameters;

class data_holder
{
public:
	data_holder::data_holder(base_parameters * set_bp);
	data_holder::~data_holder(void);

	// Variables
	base_parameters * p_bp;

	gsl_matrix * simulation_forces;
	gsl_matrix * cb_forces;
	gsl_matrix * passive_forces;
	gsl_matrix * viscous_forces;
	gsl_matrix * hs_lengths;
	gsl_matrix * command_lengths;
	gsl_matrix * slack_lengths;
	gsl_matrix * f_activated;
	gsl_matrix * f_overlap;
	gsl_matrix * f_bound;
	gsl_matrix * series_extension;
	gsl_matrix * muscle_length;
	gsl_matrix * df_matrix;
	gsl_matrix * state_forces [M_MAX_NO_OF_ISOFORMS];
	gsl_matrix * state_pops [M_MAX_NO_OF_ISOFORMS];

	gsl_matrix * circulation_volumes [CIRCULATION_MAX_NO_OF_COMPARTMENTS];
	gsl_matrix * circulation_pressures [CIRCULATION_MAX_NO_OF_COMPARTMENTS];
	
	//gsl_matrix * temp;

	// Functions
	void dump_gsl_matrix(gsl_matrix * m, char * output_file_string);
};




