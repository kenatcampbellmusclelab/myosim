#include <windows.h>

typedef struct {
    LARGE_INTEGER start;
    LARGE_INTEGER stop;
} stopWatch;

class CStopWatch {

private:
	// stopWatch timer;
    LARGE_INTEGER frequency;
    double LIToSecs( LARGE_INTEGER & L) ;
public:
	stopWatch timer;
    CStopWatch() ;
	// Added by Ken
	int no_of_calls;
	// End Ken
    void startTimer( ) ;
    void stopTimer( ) ;
    double getElapsedTime() ;
};