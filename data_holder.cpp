// data_holder.cpp
#include <stdafx.h>

#include "data_holder.h"
#include "base_parameters.h"

#include "gsl/gsl_vector.h"
#include "gsl/gsl_matrix.h"

// Constructor
data_holder::data_holder(base_parameters * set_p_bp)
{
	// Set variables
	p_bp = set_p_bp;

	// Local variables
	int i;

	int no_of_columns = p_bp->no_of_conditions * p_bp->no_of_half_sarcomeres;

	// Workout 

	// Allocate matrices
	simulation_forces=gsl_matrix_alloc(p_bp->no_of_time_points,no_of_columns);
	cb_forces=gsl_matrix_alloc(p_bp->no_of_time_points,no_of_columns);
	passive_forces=gsl_matrix_alloc(p_bp->no_of_time_points,no_of_columns);
	viscous_forces=gsl_matrix_alloc(p_bp->no_of_time_points,no_of_columns);
	hs_lengths=gsl_matrix_alloc(p_bp->no_of_time_points,no_of_columns);
	command_lengths=gsl_matrix_alloc(p_bp->no_of_time_points,no_of_columns);
	slack_lengths=gsl_matrix_alloc(p_bp->no_of_time_points,no_of_columns);
	f_activated=gsl_matrix_alloc(p_bp->no_of_time_points,no_of_columns);
	f_overlap=gsl_matrix_alloc(p_bp->no_of_time_points,no_of_columns);
	f_bound=gsl_matrix_alloc(p_bp->no_of_time_points,no_of_columns);
	df_matrix=gsl_matrix_alloc(p_bp->no_of_time_points,(no_of_columns*HS_DF_ACTIVATED_TERMS));
	series_extension=gsl_matrix_alloc(p_bp->no_of_time_points,no_of_columns);
	muscle_length=gsl_matrix_alloc(p_bp->no_of_time_points,no_of_columns);
	
	if (p_bp->circulation_mode>0)
	{
		for (i=0;i<p_bp->no_of_conditions;i++)
		{
			circulation_volumes[i] = gsl_matrix_alloc(p_bp->no_of_time_points,
										p_bp->circulation_no_of_compartments);
			circulation_pressures[i] = gsl_matrix_alloc(p_bp->no_of_time_points,
										p_bp->circulation_no_of_compartments);
		}
	}

//	temp = gsl_matrix_alloc(p_bp->no_of_time_points,no_of_columns);

	for (i=0;i<p_bp->m_no_of_isoforms;i++)
	{
		state_forces[i]=gsl_matrix_alloc(p_bp->no_of_time_points,
			no_of_columns*p_bp->no_of_states);

		state_pops[i]=gsl_matrix_alloc(p_bp->no_of_time_points,
			no_of_columns*p_bp->no_of_states);
	}
}

// Destructor
data_holder::~data_holder(void)
{
	// Tidy up

	// Variables
	int i;

	// Code
	gsl_matrix_free(simulation_forces);
	gsl_matrix_free(cb_forces);
	gsl_matrix_free(passive_forces);
	gsl_matrix_free(viscous_forces);
	gsl_matrix_free(hs_lengths);
	gsl_matrix_free(command_lengths);
	gsl_matrix_free(slack_lengths);
	gsl_matrix_free(f_activated);
	gsl_matrix_free(f_overlap);
	gsl_matrix_free(f_bound);
	gsl_matrix_free(df_matrix);
	gsl_matrix_free(series_extension);
	gsl_matrix_free(muscle_length);

//	gsl_matrix_free(temp);

	if (p_bp->circulation_mode>0)
	{
		for (i=0;i<p_bp->no_of_conditions;i++)
		{
			gsl_matrix_free(circulation_volumes[i]);
			gsl_matrix_free(circulation_pressures[i]);
		}
	}

	for (i=0;i<p_bp->m_no_of_isoforms;i++)
	{
		gsl_matrix_free(state_forces[i]);
		gsl_matrix_free(state_pops[i]);
	}
}

// Dump data
void data_holder::dump_gsl_matrix(gsl_matrix * m, char * output_file_string)
{
	// Dumps a gsl_matrix for debugging

	FILE * output_file;
	fopen_s(&output_file,output_file_string,"w");
	if (output_file==NULL)
	{
		printf("hs::dump_gsl_matrix - file %s could not be opened\n",
			output_file_string);
		printf("Now exiting\n");
		exit(1);
	}

	printf("Dumping matrix to %s\n",output_file_string);
	gsl_matrix_fprintf(output_file,m,"%g");
	fclose(output_file);
}


