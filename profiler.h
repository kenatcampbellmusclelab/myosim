// profiler.h

class CStopWatch;

class profiler
{
public:
	profiler::profiler();
	profiler::~profiler();

	// Variables
	int function_calls;
	double timer;

	CStopWatch stopwatch;

	void start_timing(void);
	void start_timing(int);
	void stop_timing(void);
};
