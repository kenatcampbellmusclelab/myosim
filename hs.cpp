// hs.cpp
#include <stdafx.h>
#include <string.h>
#include <math.h>

#include "hs.h"
#include "global_definitions.h"
#include "base_parameters.h"
#include "muscle.h"
#include "hr_timer.h"
#include "profiler.h"

#include "gsl/gsl_vector.h"
#include "gsl/gsl_matrix.h"
#include "gsl/gsl_errno.h"
#include "gsl/gsl_odeiv.h"
#include "gsl/gsl_spline.h"
#include "gsl/gsl_roots.h"
#include "gsl/gsl_rng.h"

// Function declarations for non-class members used for ODEs
int derivs(double t, const double y[], double f[], void *params);
int jacobian(double t, const double y[], double *dfdy,
	double dfdt[], void *params);
double isotonic_force_root_finder(double delta_hsl,void * params);

int derivs_counter;

// Structure for the root finder
struct root_finder_params
{
	hs * p_hs;
	double isotonic_force;
	double time_step;
};

// Structure for derivs
struct derivs_params
{
	hs* p_hs;
	int m_isoform;
};

// Constructor
hs::hs(int set_hs_number,double set_hsl, base_parameters* set_p_bp, muscle * set_p_m,
	double set_alpha_value, double set_beta_value)
{
	// Define variables
	hs_number=set_hs_number;
	hsl=set_hsl;
	p_bp=set_p_bp;
	p_parent_m=set_p_m;
	alpha_value = set_alpha_value;
	beta_value = set_beta_value;

	// Local variables
	int i;
	char temp_string[MAX_STRING_LENGTH];

	// Set check_mode
	check_mode=0;

	// Deduce Ca2+ concentration
	initial_Ca = pow(10,-p_parent_m->current_pCa);
	printf("initial_Ca %g\n",initial_Ca);

	// Create stopwatches
	for (i=0;i<NO_OF_HS_TIMERS;i++)
		p_profiler[i] = new profiler;

	// Zero the hs time
	cumulative_time=0.0;

	// Set the command length
	command_length = hsl;
	slack_length = DEFAULT_HS_SLACK_LENGTH;

	// Calculate the width of a gaussian at half-height
	gaussian_width = 2.0*sqrt(-log(0.5)/
		(gsl_vector_get(p_bp->k_cb_pos,0)/(1e18*K_BOLTZMANN*p_bp->temperature)));
	printf("gaussian_width: %g\n",gaussian_width);

	// Work out how many bins there are
	no_of_x_bins=p_bp->no_of_x_bins;

	// Now create a gsl_vector to hold the bin positions
	x = gsl_vector_alloc(no_of_x_bins);
	// Copy them from the base_parameters
	gsl_vector_memcpy(x,p_bp->x_bins);

	// Work out how many array elements we have
	n_array_length = (p_bp->no_of_attached_states*no_of_x_bins) +
						p_bp->no_of_detached_states + 1;
					// +1 allows for number of binding sites

	// Allocate space for the n_arrays, one m_isoform in each column
	n_matrix = gsl_matrix_alloc(n_array_length,p_bp->m_no_of_isoforms);

	// Dump files here in case they are needed for debugging purposes
	if ((p_parent_m->condition_number==0)&&(hs_number==0))
	{
		dump_energies();
		dump_rate_constants();

		dump_force_dependence();
	}

	// Allocate space for state_forces and pops
	cb_state_forces=gsl_matrix_alloc(p_bp->no_of_states,p_bp->m_no_of_isoforms);
	cb_state_pops=gsl_matrix_alloc(p_bp->no_of_states,p_bp->m_no_of_isoforms);

	// Set them to default values
	gsl_matrix_set_zero(cb_state_forces);
	gsl_matrix_set_zero(cb_state_pops);

	// Display values
	printf("alpha_value %g\n",alpha_value);
	printf("beta_value %g\n",beta_value);

	// Initialise the populations
	gsl_matrix_set_zero(n_matrix);
	for (i=0;i<p_bp->m_no_of_isoforms;i++)
	{
		gsl_matrix_set(n_matrix,0,i,
			gsl_matrix_get(p_bp->m_isoform_rel_populations,
				p_parent_m->condition_number,i));
	}

	// Calculate some forces
	calculate_forces_and_pops(0.0,0.0,1);

	// Dump status to file if required
	if (p_bp->debug_dump_mode>0)
	{
		dump_status_to_file(0);
	}
}

// Destructor
hs::~hs(void)
{
	// Release vectors and matrices that were allocated
	int i;

	gsl_vector_free(x);

	gsl_matrix_free(n_matrix);

/*	for (i=0;i<p_bp->m_no_of_isoforms;i++)
	{
		gsl_matrix_free(r_matrix[i]);
		gsl_matrix_free(a_matrix[i]);
	}
*/

	gsl_matrix_free(cb_state_forces);
	gsl_matrix_free(cb_state_pops);

	for (i=0;i<NO_OF_HS_TIMERS;i++)
		delete p_profiler[i];
}

void hs::implement_time_step(double time_step,double delta_hsl,
	int evolve_only, int no_evolve)
{
	// Implements a time-step
	// Evolves the cb distributions for time-step and then
	// changes the hsl by delta_hsl
	// cbs move delta_hsl*(p_bp->filament_compliance_factor)

	// Variables
	int i;
	int j;

	double temp;

	// Code

	// Some checking
	if (p_parent_m->t_counter==1)
	{
		printf("hs_length %g initial_hs_length %g ld_factor %g\n",
			hsl,p_bp->initial_hs_length,return_ld_factor(1));
	}

	if (no_evolve != 1)
	{
		p_profiler[TIMER_EVOLVE_CB_POPULATIONS]->start_timing();

		for (i=0;i<p_bp->m_no_of_isoforms;i++)
		{
			evolve_kinetics(time_step,i);
		}
		p_profiler[TIMER_EVOLVE_CB_POPULATIONS]->stop_timing();
	}

	if (evolve_only == 1)
	{
		return;
	}

	p_profiler[TIMER_SHIFT_CB_DISTRIBUTIONS]->start_timing();
	for (i=0;i<p_bp->m_no_of_isoforms;i++)
	{
		shift_cb_distributions(delta_hsl,i);

		// Correct for the fact that we might have pulled cross-bridges off the distribution
		// These should reappear as unbound heads in state 0
		temp = 0.0;
		for (j=0;j<(n_array_length-1);j++)
			temp = temp + gsl_matrix_get(n_matrix,j,0);
		temp = temp-1.0;

		gsl_matrix_set(n_matrix,0,i,gsl_matrix_get(n_matrix,0,i)-temp);
	}
	p_profiler[TIMER_SHIFT_CB_DISTRIBUTIONS]->stop_timing();

	hsl=hsl+delta_hsl;
	calculate_forces_and_pops(0.0,sliding_velocity,1);

	f_activated = gsl_matrix_get(n_matrix,n_array_length-1,0);
	f_overlap = return_f_overlap(hsl);

	double * y = new double [n_array_length];
	for (i=0;i<n_array_length;i++)
		y[i] = gsl_matrix_get(n_matrix,i,0);
	f_bound = return_n_bound(y);
	delete [] y;

	// Add to time
	cumulative_time = cumulative_time + time_step;

	// Dump to file if required
	if (p_bp->debug_dump_mode>0)
	{
		dump_status_to_file(1);
	}
}

double hs::generic_rate(int rate_index,double x, int m_isoform)
{
	// Returns a rate value

	// Variables
	int i,j;

	int old_state;
	int new_state;

	double rate=0;
	double attachment_width;

	double amp1;

	double x_mid;

	double old_extension;
	double new_extension;

	double old_energy;
	double new_energy;
	int reverse_rate_index;
	double reverse_rate;

	double ligand_modulator;

	double * fp;

	double temp;
	double temp_double;

	double cond_rate_mod;
	int cond_rate_copy;

	double cond_fd_rate_mod;
	int cond_fd_rate_copy;
	double cond_fd_rate_factor;

	// Code

	// Create space for the function_parameters
	fp = new double [p_bp->no_of_function_parameters+1];

	// Allocate the values
	for (i=1;i<=p_bp->no_of_function_parameters;i++)
	{
		fp[i]=gsl_matrix_get(p_bp->function_parameters[m_isoform],rate_index-1,i-1);
	}

	// Adjust for Ca_function_modifications
	if (p_bp->base_Ca_concentration>0.0)
	{
		for (i=1;i<=p_bp->no_of_function_parameters;i++)
		{
			fp[i] = fp[i] * GSL_MAX(0.0,
				1.0 +
				(gsl_matrix_get(p_bp->Ca_function_modifications,rate_index-1,i-1) *
						(initial_Ca-p_bp->base_Ca_concentration)/p_bp->base_Ca_concentration));
		}
	}

	// Adjust for conditions
	for (i=1;i<=p_bp->no_of_function_parameters;i++)
	{
		cond_rate_copy = (int)(floor(0.5+gsl_matrix_get(p_bp->cond_rate_copy[p_parent_m->condition_number],rate_index-1,i-1)));
		
		if (cond_rate_copy==0)
		{
			// There's no copy to worry about
			// Just set the cond_rate_mode to the matrix value
			temp_double = gsl_matrix_get(p_bp->cond_rate_mod[p_parent_m->condition_number],rate_index-1,i-1);
		}
		else
		{
//printf("cond_rate_copy %i\n",cond_rate_copy);
			// Use the scaling factor for the specified rate
			temp_double = gsl_matrix_get(p_bp->cond_rate_mod[cond_rate_copy-1],rate_index-1,i-1);
		}

		if (temp_double>0.0)
		{
			fp[i] = fp[i] * temp_double;
		}
	}

	// Adjust for current hsl
	for (i=1;i<=p_bp->no_of_function_parameters;i++)
	{
		temp_double = 1.0 + gsl_matrix_get(p_bp->ld_rate,rate_index-1,i-1) *
					  			((hsl - p_bp->initial_hs_length)/p_bp->initial_hs_length);

		if (temp_double<0.0)
			temp_double = 0.0;


		fp[i] = fp[i] * temp_double;
	}

	// Adjust for current force
	for (i=1;i<=p_bp->no_of_function_parameters;i++)
	{
		// Try to set the cond_fd_rate_factor
		cond_fd_rate_copy = (int)(floor(0.5+gsl_matrix_get(p_bp->cond_fd_rate_copy[p_parent_m->condition_number],rate_index-1,i-1)));

if (cond_fd_rate_copy!=0)
{
	printf("Ken was here\n");
	exit(1);
}

		if (cond_fd_rate_copy==0)
		{
			// There's no copy to worry about
			cond_fd_rate_factor = gsl_matrix_get(p_bp->cond_fd_rate_mod[p_parent_m->condition_number],rate_index-1,i-1);
		}
		else
		{
			cond_fd_rate_factor = gsl_matrix_get(p_bp->cond_fd_rate_mod[cond_fd_rate_copy-1],rate_index-1,i-1);
		}

/*
if ((rate_index==1)&&(i==1))
	printf("force %g  cond_fd_rate_factor :%g\n",actual_forces.total_force,cond_fd_rate_factor);
*/

		if (gsl_matrix_get(p_bp->fd_rate,rate_index-1,12)>=0.0)
		{
			temp_double = 1.0 + cond_fd_rate_factor * gsl_matrix_get(p_bp->fd_rate,rate_index-1,i-1) *
								actual_forces.total_force;
		}
		else
		{
			if (gsl_matrix_get(p_bp->fd_rate,rate_index-1,12)== -1)
				temp_double = 1.0 + cond_fd_rate_factor * gsl_matrix_get(p_bp->fd_rate,rate_index-1,i-1) *
									actual_forces.cb_force;
			else
			{
				temp_double = 1.0 + cond_fd_rate_factor * gsl_matrix_get(p_bp->fd_rate,rate_index-1,i-1) *
									actual_forces.passive_force;
				//printf("Here\n");
			}
		}

		if ((gsl_matrix_get(p_bp->fd_coefs,rate_index-1,0)>0)||(gsl_matrix_get(p_bp->fd_coefs,rate_index-1,0)<0))
		{
			temp_double = 1.0 + 
				gsl_matrix_get(p_bp->fd_coefs,rate_index-1,0)*
					(1-exp(-gsl_matrix_get(p_bp->fd_coefs,rate_index-1,1)*actual_forces.total_force));
		}

		if (temp_double<0.0)
			temp_double = 0.0;

		fp[i] = fp[i] * temp_double;
	}

	// Adjust for br_slopes
	for (i=1;i<=p_bp->no_of_function_parameters;i++)
	{
		fp[i] = fp[i] * GSL_MAX(0.0,return_br_factor(rate_index-1,i-1));
	}

	// First scan the scheme to deduce the relevant information
	for (i=0;i<p_bp->no_of_states;i++)
	{
		for (j=0;j<MAX_PATHWAYS;j++)
		{
			if (p_bp->state_rate_functions[i][j]==rate_index)
			{
				old_state=i+1;
				new_state=p_bp->state_transitions[i][j];
			}
		}
	}

	old_extension=gsl_matrix_get(p_bp->cb_extensions,old_state-1,m_isoform);
	new_extension=gsl_matrix_get(p_bp->cb_extensions,new_state-1,m_isoform);

	// Set the rates
	if (strcmp(p_bp->function_type[rate_index-1],"gaussian")==0)
	{
		amp1 = fp[1];

		if (x>=0)
		{
			rate = amp1 * exp((-0.5 * gsl_vector_get(p_bp->k_cb_pos,m_isoform) 
									* pow(x+old_extension,2.0)) /
								(1e18 * K_BOLTZMANN * p_bp->temperature));
		}
		else
		{
			if (fp[4]>=0)
			{
				rate = amp1 * exp((-0.5 * gsl_vector_get(p_bp->k_cb_neg,m_isoform)
									* pow(x+old_extension,2.0)) /
								(1e18 * K_BOLTZMANN * p_bp->temperature));
			}
			else
			{
				rate = amp1 * exp((-0.5 * gsl_vector_get(p_bp->k_cb_pos,m_isoform)
									* pow(x+old_extension,2.0)) /
								(1e18 * K_BOLTZMANN * p_bp->temperature));
			}

			if (fp[5]>0)
			{
				rate = amp1 * exp((-0.5 * fp[5]* gsl_vector_get(p_bp->k_cb_pos,m_isoform)
									* pow(x+old_extension,2.0)) /
								(1e18 * K_BOLTZMANN * p_bp->temperature));
			}
		}

		// Special cases
		if (fp[3]!=0)
		{
			rate = amp1 * exp((-0.5 * fp[3] * gsl_vector_get(p_bp->k_cb_pos,m_isoform)
									* pow(x+old_extension,2.0)) /
								(1e18 * K_BOLTZMANN * p_bp->temperature));
		}

		// Special case
		if (fp[6]!=0)
		{
			rate = amp1 * exp((-0.5 * gsl_vector_get(p_bp->k_cb_pos,m_isoform)
									* pow(x+old_extension+fp[6],2.0)) /
								(1e18 * K_BOLTZMANN * p_bp->temperature));
		}

		if ((fp[3]!=0.0)&&(fp[6]!=0.0))
		{
			rate = amp1 * exp((-0.5 * fp[3] * gsl_vector_get(p_bp->k_cb_pos,m_isoform)
									* pow(x+old_extension+fp[6],2.0)) /
								(1e18 * K_BOLTZMANN * p_bp->temperature));
		}
	}

	if (strcmp(p_bp->function_type[rate_index-1],"constant")==0)
	{
		rate = fp[1];
	}

	if (strcmp(p_bp->function_type[rate_index-1],"huxley")==0)
	{
		attachment_width=fp[2];

		if (x>0)
		{
			// Limited range
			if (x<=fp[2])
			{
				rate = fp[1] * x;
			}
			else
			{
				// biphasic
				if (x<=fp[2])
				{
					rate = fp[1]*x;
				}
				else
				{
					rate = fp[5];
				}
			}
		}
		else
		{
			rate = fp[3]+fabs(fp[6]*x);
		}
		
		if (fp[7]>0)
		{
			rate=fp[3]*(1.0/(1.0+exp(fp[7]*x)));
			rate=rate+fp[1]*x*
				(-1.0+(1.0/(1.0+exp(fp[7]*(x-fp[2])))) +
					(1.0/(1.0+exp(-fp[7]*x))));
		}
	}

	if (strcmp(p_bp->function_type[rate_index-1],"square")==0)
	{
		if (fp[13]<0)
			x_mid = -old_extension;
		else
			x_mid = 0;

		if (x>x_mid)
		{
			if (fp[4]<=0)
			{
				// Limited range
				if ((x-x_mid)<fp[2])
				{
					rate = fp[1];
				}
				else
				{
					rate = 0.0;
				}
			}
			else
			{
				// biphasic
				if ((x-x_mid)<=fp[2])
				{
					rate = fp[1];
				}
				else
				{
					rate = fp[5];
				}
			}
		}
		else
		{
			rate = fp[3];
		}
	}

	if (strcmp(p_bp->function_type[rate_index-1],"exponential")==0)
	{
		if (fp[13]>0)
			x_mid = fp[5];
		else
			x_mid = - old_extension;

		x = x - x_mid;

		rate = fp[1];

		if (x>fp[4])
		{
			rate = rate + exp(fp[2]*(x-fp[4])) - 1.0;
		}
		else
		{
			if (fp[12]<0)
				fp[3]=fp[2];

			if (x<-fp[4])
				rate = rate + exp(fp[3]*fabs(x+fp[4])) - 1.0;
		}
	}

	if (strcmp(p_bp->function_type[rate_index-1],"poly")==0)
	{
		if (fp[13]>=0)
			x_mid = -old_extension;
		else
			x_mid = 0.0+fp[14];

		x = x-x_mid;

		rate = fp[1];

		if (x > fp[2])
			rate = rate + fp[3] * fabs(pow(x-fp[2],fp[4]));
		else
		{
			if (fp[12]<0.0)
			{
				fp[8]=fp[2];
				fp[9]=fp[3];
				fp[10]=fp[4];
			}

			if (x< - fp[8])
			{
				if (fp[11]>0)
				{
					if (fp[11]>1)
						rate = fp[11];
					else
						rate = rate + fp[3] * pow(x-fp[2],fp[4]);
				}
				else
					rate = rate + fp[9] * (pow(fabs(x+fp[8]),fp[10]));
			}
		}
	}
	
	if (strcmp(p_bp->function_type[rate_index-1],"poly_ligand")==0)
	{
		x_mid = -old_extension;
		x = x-x_mid;

		// Base line
		rate = fp[1]/fp[6];

		if (x > fp[2])
			rate = rate + fp[3]/fp[7] * pow(x-fp[2],fp[4]);
		else
		{
			rate = rate + fp[9]/fp[13] * pow(fabs(x+fp[8]),fp[10]);
		}
	}

	if (strcmp(p_bp->function_type[rate_index-1],"wall")==0)
	{
		x_mid = -old_extension;

		// Base line
		rate = fp[1];

		if ((x-x_mid)>fp[2])
		{
			if (fp[3]>0)
				rate = fp[3];
			else
				rate = VERY_HIGH_RATE;
		}
		
		if ((x-x_mid)<-fp[4])
		{
			if (fp[5]>0)
				rate = fp[5];
			else
				rate = VERY_HIGH_RATE;
		}
	}

	if (strcmp(p_bp->function_type[rate_index-1],"bilog")==0)
	{
		if (fp[13]<=0)
			x_mid = old_extension;
		else
			if (fp[13]>1.0)
				x_mid = old_extension / 2.0;
			else
				x_mid = 0.0;
				
		double slope=fp[6];

		if (fp[12]<0)
		{
			fp[4]=fp[2];
			fp[5]=fp[3];
		}

		if (x>=-x_mid)
		{
			rate = fp[1] + fp[2]/(1+exp(-slope*(x+(x_mid-fp[3]))));
		}
		else
		{
			if (fabs(fp[7])>0)
			{
				rate = fp[1] + fp[4]/(1+exp(fp[7]*(x+(x_mid+fp[5]))));
			}
			else
				rate = fp[1] + fp[4]/(1+exp(slope*(x+(x_mid+fp[5]))));
		}
	}

	if (strcmp(p_bp->function_type[rate_index-1],"sigmoid")==0)
	{
		if (fp[4]<=0)
			x_mid = (old_extension+new_extension)/2.0;
		else
			x_mid = 0;

		rate = fp[3]+(fp[1] / (1.0 + exp( fp[2] * (x + x_mid + fp[5]))));
	}


	if (strcmp(p_bp->function_type[rate_index-1],"wall_ligand")==0)
	{
		x_mid = -old_extension;

		// Base line
		rate = fp[1]/fp[6];

		if ((x-x_mid)>fp[2])
			rate = VERY_HIGH_RATE/fp[6];
		
		if ((x-x_mid)<-fp[3])
			rate = VERY_HIGH_RATE/fp[6];
	}

	if (strcmp(p_bp->function_type[rate_index-1],"energy")==0)
	{
		old_energy = return_cb_energy(old_state-1,x,m_isoform);
		new_energy = return_cb_energy(new_state-1,x,m_isoform);

		reverse_rate_index = (int)fp[1];

		reverse_rate = generic_rate(reverse_rate_index,x,m_isoform);

		rate = reverse_rate * exp((old_energy-new_energy)/p_bp->beta);
	}

	// Modulate the rate by the ligand
	ligand_modulator = (double)1.0;

	if (!strcmp(p_bp->function_ligand[rate_index-1],"ATP"))
	{
		ligand_modulator=p_parent_m->current_ATP_concentration;
	}
	if (!strcmp(p_bp->function_ligand[rate_index-1],"ADP"))
	{
		ligand_modulator=p_parent_m->current_ADP_concentration;
	}
	if (!strcmp(p_bp->function_ligand[rate_index-1],"Pi"))
	{
		ligand_modulator=p_parent_m->current_Pi_concentration;
	}

	rate = rate*ligand_modulator;

	// Limit rates
	if (rate>VERY_HIGH_RATE)
		rate=VERY_HIGH_RATE;

	if (rate<0.0)
		rate=0.0;
	
	// Tidy up
	delete [] fp;

	// Return
	return rate;
}


double hs::return_cb_energy(int cb_state, double x, int m_isoform)
{
	// Returns the energy of a cb

	// Variables
	double k_link;
	double energy;

	if (p_bp->state_attached[cb_state]==0)
	{
		// state is detached
		energy = gsl_matrix_get(p_bp->cb_base_energies,cb_state,m_isoform) * p_bp->beta;
	}
	else
	{
		// state is attached
		if (x > - gsl_matrix_get(p_bp->cb_extensions,cb_state,m_isoform))
		{
			k_link = gsl_vector_get(p_bp->k_cb_pos,m_isoform);
		}
		else
		{
			k_link = gsl_vector_get(p_bp->k_cb_neg,m_isoform);
		}

		energy = (gsl_matrix_get(p_bp->cb_base_energies,cb_state,m_isoform) * p_bp->beta) +
			(0.5* k_link * pow((x+gsl_matrix_get(p_bp->cb_extensions,cb_state,m_isoform)),2.0));
	}

	return energy;
}

void hs::dump_gsl_matrix(gsl_matrix * m, char * output_file_string)
{
	// Dumps a gsl_matrix for debugging

	FILE * output_file;
	fopen_s(&output_file,output_file_string,"w");
	if (output_file==NULL)
	{
		printf("hs::dump_gsl_matrix - file %s could not be opened\n",
			output_file_string);
		printf("Now exiting\n");
		exit(1);
	}

	printf("Dumping gsl_matrix to %s\n",output_file_string);
	gsl_matrix_fprintf(output_file,m,"%g");
	printf("Finished dumping gsl_matrix to %s\n",output_file_string);
	fclose(output_file);
}
/*
void hs::set_N(double time_step)
{
	// Code returns the number of sites that are available

	// Variables

	// Code
	set_f_overlap();
	set_f_bound();
	set_f_activated(time_step);
	
	N=f_activated;

if (check_mode)
{
	printf("Set_N: f_overlap: %g  f_bound: %g  f_activated: %g  N: %g\n",
		f_overlap,f_bound,f_activated,N);
}
}
*/

double hs::return_f_overlap(double x)
{
	// Code returns f_overlap, the proportion of cross-bridges that can bind
	// at half-sarcomere length s

	// Variables
	double x_overlap;
	double x_no_overlap;
	double max_x_overlap;

	// Code
	x_no_overlap = x - p_bp->hs_thick_filament_length;
	x_overlap = p_bp->hs_thin_filament_length - x_no_overlap;
	max_x_overlap = p_bp->hs_thick_filament_length - p_bp->hs_bare_zone_length;

	if (x_overlap<0.0)
		f_overlap = 0.0;

	if ((x_overlap>0.0) && (x_overlap<=max_x_overlap))
		f_overlap = x_overlap/max_x_overlap;

	if (x_overlap>max_x_overlap)
		f_overlap = 1.0;

	if (x<p_bp->hs_thin_filament_length)
	{
		f_overlap = 1.0 + (p_bp->hs_k_falloff*(x-p_bp->hs_thin_filament_length));
		if (f_overlap<0.0)
			f_overlap=0.0;
	}

	return f_overlap;
}

/*
void hs::set_f_overlap(void)
{
	// Code sets f_overlap, the proportion of cross-bridges that can bind
	// at this half-sarcomere length

	// Variables
	double x_overlap;
	double x_no_overlap;
	double max_x_overlap;

	// Code
	x_no_overlap = hsl - p_bp->hs_thick_filament_length;
	x_overlap = p_bp->hs_thin_filament_length - x_no_overlap;
	max_x_overlap = p_bp->hs_thick_filament_length - p_bp->hs_bare_zone_length;

	if (x_overlap<0.0)
		f_overlap = 0.0;

	if ((x_overlap>0.0) && (x_overlap<=max_x_overlap))
		f_overlap = x_overlap/max_x_overlap;

	if (x_overlap>max_x_overlap)
		f_overlap = 1.0;

	if (hsl<p_bp->hs_thin_filament_length)
	{
		f_overlap = 1.0 + (p_bp->hs_k_falloff*(hsl-p_bp->hs_thin_filament_length));
		if (f_overlap<0.0)
			f_overlap=0.0;
	}

//printf("hs_k_falloff %g\n",p_bp->hs_k_falloff);

	// Adjust for alpha value
	f_overlap = f_overlap * alpha_value;
}

void hs::set_f_bound(void)
{
	// Code sets f_bound,the proportion of sites that are bound

	// Variables
	int i,j;

	// Code
	f_bound = 0.0;

	for (i=0;i<p_bp->m_no_of_isoforms;i++)
	{
		for (j=0;j<p_bp->no_of_states;j++)
		{
			if (p_bp->state_attached[j])
			{
				f_bound = f_bound + gsl_matrix_get(cb_state_pops,j,i);
			}
		}
	}

	// Catch
	if (f_bound>1.0)
		f_bound=1.0;
}

void hs::set_f_activated(double time_step)
{
	// Code sets f_activated, the proportion of sites that are
	// available for binding

	// Variables
	double Ca_inc_factor = 1.0;
	double Ca_dec_factor = 1.0;

	double Ca_plus_factor = 1.0;
	double Ca_minus_factor = 1.0;

	double temp_double;

	int i;

	// Code

	// Calculate df

	for (i=0;i<4;i++)
	{
		temp_double = gsl_matrix_get(p_bp->cond_scaling_factor,p_parent_m->condition_number,i);

		switch (i)
		{
		case 0:
			Ca_inc_factor = temp_double * return_ld_factor(i) * return_fd_factor(i);
			break;
		case 1:
			Ca_dec_factor = temp_double * return_ld_factor(i) * return_fd_factor(i);
			break;
		case 2:
			Ca_plus_factor = temp_double * return_ld_factor(i) * return_fd_factor(i);
			break;
		case 3:
			Ca_minus_factor = temp_double * return_ld_factor(i) * return_fd_factor(i);
			break;
		default:
			temp_double=temp_double;
		}
	}

	/*
	if (p_bp->base_Ca_concentration>0)
	{
		Ca_inc_factor = GSL_MAX(0.0,
					1.0 +
					gsl_vector_get(p_bp->Ca_scaling_factor,0) *
						(initial_Ca-p_bp->base_Ca_concentration)/p_bp->base_Ca_concentration);

		temp_double = gsl_matrix_get(p_bp->cond_Ca_scaling_factor,p_parent_m->condition_number,0);
		if (temp_double>0.0)
		{
			Ca_inc_factor = Ca_inc_factor * temp_double;
		}

		Ca_dec_factor = GSL_MAX(0.0,
					1.0 +
					gsl_vector_get(p_bp->Ca_scaling_factor,1) *
						(initial_Ca-p_bp->base_Ca_concentration)/p_bp->base_Ca_concentration);

		temp_double = gsl_matrix_get(p_bp->cond_Ca_scaling_factor,p_parent_m->condition_number,1);
		if (temp_double>0.0)
		{
			Ca_dec_factor = Ca_dec_factor * temp_double;
		}


		Ca_plus_factor = GSL_MAX(0.0,
					1.0 +
					gsl_vector_get(p_bp->Ca_scaling_factor,2) *
						(initial_Ca-p_bp->base_Ca_concentration)/p_bp->base_Ca_concentration);

		temp_double = gsl_matrix_get(p_bp->cond_Ca_scaling_factor,p_parent_m->condition_number,2);
//printf("Condition: %i before_plus_factor %g\n",p_parent_m->condition_number,Ca_plus_factor);
//printf("condition: %i temp_double: %g\n",p_parent_m->condition_number,temp_double);
		if (temp_double>0.0)
		{
			Ca_plus_factor = Ca_plus_factor * temp_double;
		}
//printf("Condition: %i after_plus_factor %g\n",p_parent_m->condition_number,Ca_plus_factor);


		Ca_minus_factor =  GSL_MAX(0.0,
					1.0 +
					gsl_vector_get(p_bp->Ca_scaling_factor,3) *
						(initial_Ca-p_bp->base_Ca_concentration)/p_bp->base_Ca_concentration);

		temp_double = gsl_matrix_get(p_bp->cond_Ca_scaling_factor,p_parent_m->condition_number,3);
		if (temp_double>0.0)
		{
			Ca_minus_factor = Ca_minus_factor * temp_double;
		}

	}
	else
	{
		Ca_inc_factor = 1.0;
		Ca_dec_factor = 1.0;
		Ca_plus_factor = 1.0;
		Ca_minus_factor = 1.0;
	}

	/*
	// Adjust for conditions
	for (i=0;i<4;i++)
	{
		temp_double = gsl_matrix_get(
			p_bp->cond_Ca_scaling_factor,p_parent_m->condition_number,i);

		if (temp_double > 0)
		{
			switch (i)
			{
				case 0:
					Ca_inc_factor = Ca_inc_factor * temp_double;
					break;
				case 1:
					Ca_dec_factor = Ca_dec_factor * temp_double;
					break;
				case 2:
					Ca_plus_factor = Ca_plus_factor * temp_double;
					break;
				case 3:
					Ca_dec_factor = Ca_dec_factor * temp_double;
					break;
				default:
					temp_double=temp_double;
			}
		}
	}
*/
/*
if (p_parent_m->t_counter==1)
{
printf("Condition_number: %i\n",p_parent_m->condition_number);
printf("Ca_inc_factor: %g\n",Ca_inc_factor);
printf("Ca_dec_factor: %g\n",Ca_dec_factor);
printf("Ca_plus_factor: %g\n",Ca_plus_factor);
printf("Ca_minus_factor: %g\n",Ca_minus_factor);
}
	
	double current_Ca;
	current_Ca=pow(10.0,-p_parent_m->current_pCa);

	if (current_Ca>p_bp->Ca_threshold)
		df_inc = Ca_inc_factor * 
				p_bp->a_on_rate * (pow(10.0,-p_parent_m->current_pCa) - p_bp->Ca_threshold) *
						(f_overlap-f_activated);
	else
		df_inc = 0.0;

	// Decrement
	df_dec = Ca_dec_factor * p_bp->a_off_rate  * (f_activated-f_bound);

	// Cooperativity
	if (p_bp->dN_mode > 0.0)
	{
		df_coop = Ca_plus_factor *p_parent_m->m_k_plus * f_bound -
				Ca_minus_factor * p_parent_m->m_k_minus * (f_overlap-f_activated);
	}

	if (p_bp->dN_mode == -1.0)
	{
		df_coop = p_parent_m->m_k_plus * f_bound * (f_overlap-f_activated) -
				p_parent_m->m_k_minus * (f_overlap-f_activated) * (f_activated-f_bound);
	}

	if (p_bp->dN_mode == -2.0)
	{
		df_coop = p_parent_m->m_k_plus * 
			(gsl_pow_2(f_activated) * (f_overlap - f_activated) -
				gsl_pow_2(f_overlap - f_activated) * f_activated);
	}

	// Force-based recruitment
	df_recruit = (p_bp->k_recruit_tf * actual_forces.total_force) +
			(p_bp->k_recruit_cbf * actual_forces.cb_force) +
			(p_bp->k_recruit_pf * actual_forces.passive_force);


	// Euler step
	f_activated = f_activated + time_step * 
		(df_inc - df_dec + df_coop + df_recruit);

// Check for isnan
if (gsl_isnan(f_activated))
{
	// This is a problem for MATLAB
	printf("isnan error in hs::set_f_bound\n");
	printf("df_inc: %g\n",df_inc);
	printf("df_dec: %g\n",df_dec);
	printf("df_coop: %g\n",df_coop);
	CopyFile(p_bp->instruction_file_string,"instruction_file_crash_fact.txt",FALSE);
}


	// Limit
	if (f_activated<f_bound)
		f_activated=f_bound;
	if (f_activated>f_overlap)
		f_activated=f_overlap;
	if (f_activated<0)
		f_activated=0.0;
}
*/

double hs::return_ld_factor(int i)
{
	double temp;

	if (p_parent_m->t_counter==0)
		temp = 1;
	else
	{
	/*	temp = 1.0 + gsl_vector_get(p_bp->ld_slope,i) * 
					(p_parent_m->current_length - p_parent_m->original_length) /
						p_parent_m->original_length;
						*/
/*		temp = 1.0 + gsl_vector_get(p_bp->ld_slope,i) *
				(actual_forces.passive_force/1e4);
*/
		temp = 1.0 + gsl_vector_get(p_bp->ld_slope,i) *
					(hsl - p_bp->initial_hs_length)/p_bp->initial_hs_length;
	}
					

	if (temp<0.0)
		temp = 0.0;

	return temp;
}

double hs::return_fd_factor(int i)
{
	double temp;

	if (p_parent_m->t_counter==0)
		temp = 1.0;
	else
	{
		temp = 1.0 + gsl_vector_get(p_bp->fd_slope,i) *
						actual_forces.total_force;
	}

	if (temp<0.0)
		temp = 0.0;

	return temp;
}

double hs::return_br_factor(int i,int j)
{
	double temp;

	temp = 1.0 + gsl_matrix_get(p_bp->br_slope,i,j) * 
			(gsl_vector_get(p_bp->initial_dml,p_parent_m->condition_number)/
				p_bp->initial_hs_length);

	if (temp<0.0)
		temp = 0.0;

	return temp;
}

void hs::evolve_kinetics(double time_step, int m_isoform)
{
	// Code evolves cb_populations by the time_step
	// Method copied from GSL Reference Manual Chapter 25

	// Variables
	int i;
	int loop_counter;

	double t=0.0;
	double t1=time_step;
	double h=time_step/1.0;

	double holder;

	struct derivs_params params = {this,m_isoform};

	double * y = new double [n_array_length];

double before_sum;
double after_sum;

	// Code

	// Code

	const gsl_odeiv_step_type * T = gsl_odeiv_step_rk2;

	gsl_odeiv_step * s = gsl_odeiv_step_alloc(T,n_array_length);
	gsl_odeiv_control * c = gsl_odeiv_control_y_new(
							POPULATION_ABS_TOLERANCE,POPULATION_REL_TOLERANCE);
	gsl_odeiv_evolve * e = gsl_odeiv_evolve_alloc(n_array_length);

	gsl_odeiv_system sys = {derivs,jacobian, n_array_length,&params};

	// Initialise the populations
	for (i=0;i<n_array_length;i++)
	{
		y[i] = gsl_matrix_get(n_matrix,i,m_isoform);
	}
/*before_sum = 0.0;
for (i=0;i<(n_array_length-1);i++)
	before_sum = before_sum + y[i];
	*/

	// Evolve
	loop_counter=0;
	derivs_counter=0;
	while (t<t1)
	{
		loop_counter++;

		p_profiler[TIMER_GSL_ODEIV]->start_timing();
		int status = gsl_odeiv_evolve_apply(e,c,s,&sys,&t,t1,&h,y);
		p_profiler[TIMER_GSL_ODEIV]->stop_timing();

		if (status != GSL_SUCCESS)
		{
			printf("hs::evolve_cb_populations_error\n");
			exit(1);
			break;
		}
	}

	// Update
	for (i=0;i<n_array_length;i++)
	{
		if (y[i]<0.0)
			y[i]=(double)0.0;

		gsl_matrix_set(n_matrix,i,m_isoform,y[i]);
	}
/*
after_sum = 0.0;
for (i=0;i<(n_array_length-1);i++)
	after_sum = after_sum + y[i];
printf("Time_step: %i before_sum %g after_sum %g\n",p_parent_m->t_counter,before_sum,after_sum);
if (fabs(after_sum-1.0)>0.01)
	exit(1);
*/

	// Tidy up
	delete [] y;

	gsl_odeiv_evolve_free(e);
	gsl_odeiv_control_free(c);
	gsl_odeiv_step_free(s);
}

void hs::shift_cb_distributions(double delta_hsl, int m_isoform)
{
	// This function moves the cb distributions by delta_hsl
	// Populations are shifted by compliance_factor * delta_hsl

	// Cubic spline method is copied from GSL Manual Chapter 26

	// Short-cut if there is no movement
	if (delta_hsl==(double)0.0)
	{
		return;
	}

	// Variables
	int i,j;

	double x_eval;

	double new_y;

	double shift = (p_bp->filament_compliance_factor) * delta_hsl;

	double * xx = new double [no_of_x_bins];
	double * yy = new double [no_of_x_bins];

	gsl_interp_accel * acc;
	gsl_spline * spline;

	// Code

	// Assign the x values
	for (i=0;i<no_of_x_bins;i++)
	{
		xx[i]=gsl_vector_get(x,i);
	}

	// Cycle through populations
	for (i=0;i<p_bp->no_of_states;i++)
	{
		if (p_bp->state_attached[i]==1)
		{
			// We need to move populations
			for (j=0;j<no_of_x_bins;j++)
			{
				yy[j]=gsl_matrix_get(n_matrix,p_bp->n_vector_indices[i][0]+j,m_isoform);
			}
			acc = gsl_interp_accel_alloc();
			spline = gsl_spline_alloc(gsl_interp_cspline,no_of_x_bins);
			gsl_spline_init(spline,xx,yy,no_of_x_bins);

			// Assign the new values
			for (j=0;j<no_of_x_bins;j++)
			{
				x_eval = gsl_vector_get(x,j) - shift;

				if ((x_eval>=gsl_vector_get(x,0)) &&
					(x_eval<=gsl_vector_get(x,no_of_x_bins-1)))
				{
					// x_eval is in range
					new_y = gsl_spline_eval(spline,x_eval,acc);
				}
				else
				{
					// Extrapolating beyond range
					new_y=(double)0.0;
				}


				// Prevent negative populations
				if (new_y<0.0)
					new_y=(double)0.0;

				// Assign
				gsl_matrix_set(n_matrix,p_bp->n_vector_indices[i][0]+j,m_isoform,new_y);
			}
		}
	}

	// Tidy up
	delete [] xx;
	delete [] yy;

	gsl_spline_free(spline);
	gsl_interp_accel_free(acc);
}

void hs::calculate_forces_and_pops(double delta_hsl, double sliding_velocity,
	int update_mode)
{
	// Code calculates active, passive and total forces
	// with an optional length change

p_profiler[TIMER_FORCES_AND_POPS]->start_timing();

	// Variables
	int m_counter;
	int i,j;

	double dx;
	double cb_ext;

	double f_holder;
	double n_holder;

	double n_pop;

	double cb_adjustment = delta_hsl * p_bp->filament_compliance_factor;

	double passive_Ca_factor = 1.0;
	double temp_double;

	forces_structure * fs;
		
	// Code

	// Branch depending on mode
	if (update_mode==1)
	{
		// Real update - hold data for status updates
		fs = &actual_forces;
	}
	else
	{
		// We are testing things
		fs = &estimated_forces;
	}

	// Calculate the cb force first
	fs->cb_force=0.0;

	// Cycle through the m_isoforms
	for (m_counter=0;m_counter<p_bp->m_no_of_isoforms;m_counter++)
	{
		// Cycle through the states
		for (i=0;i<p_bp->no_of_states;i++)
		{
			f_holder=0.0;
			n_holder=0.0;

			if (p_bp->state_attached[i]==1)
			{
				cb_ext = gsl_matrix_get(p_bp->cb_extensions,i,m_counter);

				for (j=0;j<no_of_x_bins;j++)
				{
					dx = gsl_vector_get(x,j) + cb_adjustment;
					n_pop = gsl_matrix_get(n_matrix,p_bp->n_vector_indices[i][0]+j,m_counter);

					n_holder = n_holder + n_pop;

					if (dx > -cb_ext)
					{
						f_holder = f_holder + 
							(n_pop * gsl_vector_get(p_bp->k_cb_multiplier,i) * 
								gsl_vector_get(p_bp->k_cb_pos,m_counter) * (dx + cb_ext));
					}
					else
					{
						f_holder = f_holder + 
							(n_pop * gsl_vector_get(p_bp->k_cb_multiplier,i) *
								gsl_vector_get(p_bp->k_cb_neg,m_counter) * (dx + cb_ext));
					}
				}

				// Correct for number density,nm units and alpha value
				f_holder = f_holder * p_bp->cb_number_density * 1e-9;

				// Adjust for variability
				f_holder = f_holder * alpha_value;

				// Ensure n_holder>=0 
				if (n_holder<0.0)
					n_holder=0.0;

				// Allocate
				if (update_mode)
				{
					gsl_matrix_set(cb_state_forces,i,m_counter,f_holder);
					gsl_matrix_set(cb_state_pops,i,m_counter,n_holder);
				}
			}
			else
			{
				// The state isn't attached
				if (update_mode)
				{
					gsl_matrix_set(cb_state_forces,i,m_counter,(double)0.0);
					gsl_matrix_set(cb_state_pops,i,m_counter,
						gsl_matrix_get(n_matrix,p_bp->n_vector_indices[i][0],m_counter));
				}
			}

			// Add in the cb force
			fs->cb_force = fs->cb_force + f_holder;
		}
	}

	// Passive force
	if (p_bp->passive_force_linear>0)
	{
		passive_Ca_factor = 1.0 + gsl_vector_get(p_bp->Ca_scaling_factor,5) *
			(initial_Ca-p_bp->base_Ca_concentration)/p_bp->base_Ca_concentration;

		fs->passive_force = (passive_Ca_factor * p_bp->passive_k_linear) * 
			(hsl + delta_hsl - p_bp->passive_hsl_slack);
	}
	else
	{
		fs->passive_force = p_bp->passive_sigma * 
				(exp((hsl + delta_hsl)/p_bp->passive_L)-1.0);

		// Forces passive force to get negative
		if ((hsl+delta_hsl)<p_bp->passive_L_slack_hsl)
			fs->passive_force = fs->passive_force +
				p_bp->passive_L_slack_k * (hsl+delta_hsl-p_bp->passive_L_slack_hsl);
	}

	if ((int)p_bp->passive_force_mode==1)
	{
		temp_double = gsl_matrix_get(p_bp->cond_scaling_factor,
			p_parent_m->condition_number,4);

		passive_Ca_factor=temp_double;

		fs->passive_force = passive_Ca_factor * p_bp->passive_k_linear * 
			(hsl + delta_hsl - 
				(p_bp->passive_hsl_slack + 
					gsl_vector_get(p_bp->d_hsl_slack,p_parent_m->condition_number)));
	}

	if ((int)p_bp->passive_force_mode==2)
	{
		fs->passive_force = p_bp->passive_sigma * 
				(exp((hsl + delta_hsl)/p_bp->passive_L)-1.0);

		// Forces passive force to get negative
		if ((hsl+delta_hsl)<p_bp->passive_L_slack_hsl)
			fs->passive_force = fs->passive_force +
				p_bp->passive_L_slack_k * (hsl+delta_hsl-p_bp->passive_L_slack_hsl);
	}

	if ((int)p_bp->passive_force_mode==3)
	{
		if ((hsl+delta_hsl)>p_bp->passive_hsl_slack)
			fs->passive_force = exp((hsl + delta_hsl - p_bp->passive_hsl_slack) /
					p_bp->passive_L) - 1.0 +
					p_bp->passive_k_linear * (hsl+delta_hsl - p_bp->passive_hsl_slack);
		else
			fs->passive_force = -exp(-(hsl + delta_hsl - p_bp->passive_hsl_slack) /
					p_bp->passive_L) + 1.0  +
					p_bp->passive_k_linear * (hsl+delta_hsl - p_bp->passive_hsl_slack);
	}

	if ((int)p_bp->passive_force_mode==4)
	{
		fs->passive_force = p_bp->passive_sigma * 
			(exp((hsl + delta_hsl)/p_bp->passive_L)-1.0) - 
			(p_bp->passive_sigma * 
			(exp(p_bp->passive_L_slack_hsl/p_bp->passive_L)-1.0));
	}

	if ((int)p_bp->passive_force_mode==5)
	{
		fs->passive_force = p_bp->passive_sigma *
			(exp((hsl+delta_hsl-p_bp->passive_hsl_slack)/p_bp->passive_L)-1);
	}

	if ((int)p_bp->passive_force_mode==6)
	{
		temp_double = gsl_matrix_get(p_bp->cond_scaling_factor,
			p_parent_m->condition_number,4);

		passive_Ca_factor=temp_double;

		fs->passive_force = passive_Ca_factor * p_bp->passive_k_linear * 
			(hsl + delta_hsl - 
				(p_bp->passive_hsl_slack + 
					gsl_vector_get(p_bp->d_hsl_slack,p_parent_m->condition_number)));

		if (fs->passive_force<0.0)
			fs->passive_force = 0.0;
	}

	// Adjust for variability
	fs->passive_force = fs->passive_force * beta_value;

	// Viscosity
	fs->viscous_force = p_bp->viscosity * sliding_velocity;

	// Total force
	fs->total_force = fs->cb_force + fs->passive_force + fs->viscous_force;

	// Check for isnan
	if (p_bp->debug_dump_mode)
	{
		if (gsl_isnan(fs->total_force))
		{
			// This is a problem for MATLAB
			printf("isnan error in hs::calculate_forces_and_pops\n");
			printf("n_holder: %g\n",n_holder);
			printf("fs->cb_force: %g\n",fs->cb_force);
			printf("fs->passive_force: %g\n",fs->passive_force);
			printf("fs->viscous_force: %g\n",fs->viscous_force);
			printf("t_counter: %i\n",p_parent_m->t_counter);
			//CopyFile(p_bp->instruction_file_string,"instruction_file_crash_cb_force.txt",FALSE);
			fs->total_force=0.0;
			p_bp->error_flag=1;
		}
	}

p_profiler[TIMER_FORCES_AND_POPS]->stop_timing();
}

double hs::return_hs_length_for_force(double isotonic_force, double time_step)
{
	// Code tries to find a length where the hs_force equals the test_force
	// Uses the GSL root-finding exapmle from section 32.10
p_profiler[TIMER_HS_LENGTH_FOR_FORCE]->start_timing();


	// Variables
	int i;
	int status;
	int iter=0;
	int max_iter=100;
	const gsl_root_fsolver_type *T;
	gsl_root_fsolver *s;
	double r=0;
	double x_lo = -ISOTONIC_FORCE_INITIAL_ADJUSTMENT;
	double x_hi = +ISOTONIC_FORCE_INITIAL_ADJUSTMENT;
	double f_lo;
	double f_hi;

	gsl_function F;
	struct root_finder_params params = {this,isotonic_force,time_step};

	F.function = &isotonic_force_root_finder;
	F.params = &params;

	// Check that we are bracketing a zero
	f_lo = isotonic_force_root_finder(x_lo,&params);
	f_hi = isotonic_force_root_finder(x_hi,&params);

	if ((GSL_SIGN(f_hi)-GSL_SIGN(f_lo))!=2)
	{
		printf("Condition %i\n",p_parent_m->condition_number);
		printf("hs::return_hs_length_for_force does not bracket a zero\n");
		printf("current_length: %g  current_force: %g\n",hsl,actual_forces.total_force);
		printf("x_lo: %g\t\tf_lo: %g\n",x_lo,f_lo);
		printf("x_hi: %g\t\tf_hi: %g\n",x_hi,f_hi);

printf("hs_total_force: %g\n",estimated_forces.total_force);
printf("hs_cb_force: %g\n",estimated_forces.cb_force);
printf("hs_passive_force: %g\n",estimated_forces.passive_force);
for (i=0;i<p_bp->no_of_states;i++)
{
	printf("state_pops[%i]: %g\n",i,gsl_matrix_get(cb_state_pops,i,0));
}
//dump_gsl_matrix(n_matrix,"n_matrix.txt");

		p_bp->error_flag=1;

		return 500.0;
	}

	T = gsl_root_fsolver_brent;
	s = gsl_root_fsolver_alloc(T);
	gsl_root_fsolver_set(s,&F,x_lo,x_hi);

	do
	{
		iter++;
		status = gsl_root_fsolver_iterate(s);
		r = gsl_root_fsolver_root(s);
		x_lo = gsl_root_fsolver_x_lower(s);
		x_hi = gsl_root_fsolver_x_upper(s);
		status = gsl_root_test_interval(x_lo,x_hi,ISOTONIC_FORCE_HSL_TOLERANCE,0);
	}
	while (status == GSL_CONTINUE && iter < max_iter);

	// Tidy up
	gsl_root_fsolver_free(s);

	// Return
	return (hsl+r);

p_profiler[TIMER_HS_LENGTH_FOR_FORCE]->stop_timing();
}

double isotonic_force_root_finder(double delta_hsl,void * params)
{
	// Function estimates the force after a length change of delta_hsl
	// and calculates the difference between that and the isotonic force

	// Variable
	struct root_finder_params *p =
		(struct root_finder_params *) params;

	double hs_force;
	double isotonic_force = p->isotonic_force;
	hs * p_hs = p->p_hs;
	double time_step = p->time_step;

	// Code
	if (delta_hsl!=0)
	{
		p_hs->calculate_forces_and_pops(delta_hsl,delta_hsl/time_step,0);
		hs_force=p_hs->estimated_forces.total_force;
	}
	else
	{
		hs_force=p_hs->actual_forces.total_force;
	}

	return (hs_force-isotonic_force);
}

// Functions calculating the derivative and the jacobian of the
// cross-bridge populations. These are called by the GSL ODE solvers
// Note that they are not members of the hs class

int derivs(double t, const double y[], double f[], void *params)
{
	// Returns the derivitives
	// cb_populations are passed in as a vector
	// The code knows about the parent hs object through a pointer
	// that is passed in through params

	// Variables
	int i,j,k;
	int n_array_length;

	int other_state;
	int rate_index;

	int bin_index;

	double n_available;
	double n_bound;
	double n_overlap;

	double rate_constant;

	double k_on_factor;
	double k_off_factor;
	double k_coop_factor;
	
	// Temps
	int i_temp;
	double d_temp;


	// Variable
	struct derivs_params *p =
		(struct derivs_params *) params;

	hs * p_hs = p->p_hs;
	int m_isoform = p->m_isoform;

	// Code
p_hs->p_profiler[TIMER_DERIVS]->start_timing();

	n_array_length = p_hs->n_array_length;

	// Set n_available and n_bound
	n_available = y[n_array_length-1];
	n_bound = p_hs->return_n_bound(y);

	// Zero derivitives
	for (i=0;i<n_array_length;i++)
		f[i]=0.0;

	// Now cycle through the states
	for (i=0;i<p_hs->p_bp->no_of_states;i++)
	{
		if (p_hs->p_bp->state_attached[i]==0)
		{
			// We are in a detached state
			for (j=0;j<MAX_PATHWAYS;j++)
			{
				other_state = p_hs->p_bp->state_transitions[i][j];

				if (other_state != 0)
				{
					// We have a transition, find the rate and deduce what to do
					rate_index = p_hs->p_bp->state_rate_functions[i][j];

					if (p_hs->p_bp->function_action[rate_index-1]=='a')
					{
						// Cross-bridges will increment into multiple elements and decrement from a single element
						i_temp = p_hs->p_bp->n_vector_indices[i][0];

						// Cycle through the bins
						bin_index = 0;

						for (k=p_hs->p_bp->n_vector_indices[other_state-1][0];
								k<=p_hs->p_bp->n_vector_indices[other_state-1][1];k++)
						{
							// Adjust the rate constant for the bin_width
							rate_constant = p_hs->p_bp->x_bin_increment *
												p_hs->generic_rate(rate_index,
													gsl_vector_get(p_hs->x,bin_index),m_isoform);

							// Calculate the adjustment
							d_temp = rate_constant * (n_available - n_bound) * y[i_temp];

							// These are the increments
							f[k] = f[k] + d_temp;

							// These are the decrements
							f[i_temp] = f[i_temp] - d_temp;

							// Increment
							bin_index++;
						}
					}

					if (p_hs->p_bp->function_action[rate_index-1]=='n')
					{
						// Cross-bridges will increment one element and decrement from a single element
						rate_constant = p_hs->generic_rate(rate_index,0,m_isoform);

						// Calculate the adjustment
						i_temp = p_hs->p_bp->n_vector_indices[i][0];
						d_temp = rate_constant * y[i_temp];

						// Increment
						k = p_hs->p_bp->n_vector_indices[other_state-1][0];
						f[k] = f[k] + d_temp;

						// Decrement
						f[i_temp] = f[i_temp] - d_temp;
					}
				}
			}
		}
		else
		{
			// We are in an attached state
			for (j=0;j<MAX_PATHWAYS;j++)
			{
				other_state = p_hs->p_bp->state_transitions[i][j];

				if (other_state != 0)
				{
					// We have a transition, find the rate and deduce what to do
					rate_index = p_hs->p_bp->state_rate_functions[i][j];

					if (p_hs->p_bp->function_action[rate_index-1]=='d')
					{
						// Cross-bridges will increment into a single element and
						// decrement from multiple elements

						i_temp = p_hs->p_bp->n_vector_indices[other_state-1][0];

						// Cycle through the bins
						bin_index = 0;

						for (k=p_hs->p_bp->n_vector_indices[i][0];
								k<=p_hs->p_bp->n_vector_indices[i][1];k++)
						{
							// Deduce the rate constant
							rate_constant = p_hs->generic_rate(rate_index,
												gsl_vector_get(p_hs->x,bin_index),m_isoform);

							// Calculate the adjustment
							d_temp = rate_constant * y[k];

							// These are the increments
							f[i_temp] = f[i_temp] + d_temp;

							// These are the decrements
							f[k] = f[k] - d_temp;

							// Increment
							bin_index++;
						}
					}

					if (p_hs->p_bp->function_action[rate_index-1]=='n')
					{
						// Cross-bridges will increment and decrement from multiple places

						// Cycle through bins
						bin_index = 0;
						
						for (k=p_hs->p_bp->n_vector_indices[i][0];
								k<=p_hs->p_bp->n_vector_indices[i][1];k++)
						{
							rate_constant = p_hs->generic_rate(rate_index,
												gsl_vector_get(p_hs->x,bin_index),m_isoform);

							// Calculate the adjustment
							d_temp = rate_constant * y[k];

							// These are the increments
							i_temp = p_hs->p_bp->n_vector_indices[other_state-1][0]+bin_index;
							f[i_temp] = f[i_temp] + d_temp;

							// These are the decrements
							f[k] = f[k] - d_temp;

							// Increment counter
							bin_index++;
						}
					}
				}			
			}
		}
	}

	// Update n_available
	i_temp = p_hs->n_array_length-1;
//p_hs->check_int(i_temp);

	n_overlap = p_hs->return_f_overlap(p_hs->hsl);
	int coop_power = (int)(p_hs->p_bp->coop_power);

	// Set k_on_factor, determined by 0th element of p_bp->thin_function_mod
	int cond_number = p_hs->p_parent_m->condition_number;
	int copy_number = (int)gsl_matrix_get(p_hs->p_bp->thin_function_copy,cond_number,0);

	if (copy_number>0)
	{
		k_on_factor = gsl_matrix_get(p_hs->p_bp->thin_function_mod,copy_number-1,0);
	}
	else
	{
		k_on_factor = gsl_matrix_get(p_hs->p_bp->thin_function_mod,cond_number,0);
	}

	// k_off_factor, determined by 1st element of p_bp->thin_function_mod
	copy_number = (int)gsl_matrix_get(p_hs->p_bp->thin_function_copy,cond_number,1);

	if (copy_number>0)
	{
		k_off_factor = gsl_matrix_get(p_hs->p_bp->thin_function_mod,copy_number-1,1);
	}
	else
	{
		k_off_factor = gsl_matrix_get(p_hs->p_bp->thin_function_mod,cond_number,1);
	}

	// k_coop_factor, determined by 2nd element of p_bp->thin_function_mod
	copy_number = (int)gsl_matrix_get(p_hs->p_bp->thin_function_copy,cond_number,2);

	if (copy_number>0)
	{
		k_coop_factor = gsl_matrix_get(p_hs->p_bp->thin_function_mod,copy_number-1,2);
	}
	else
	{
		k_coop_factor = gsl_matrix_get(p_hs->p_bp->thin_function_mod,cond_number,2);
	}

//printf("Cond %i k_on_Ca_factor: %g\n",cond_number,k_on_Ca_factor);

double Ca_factor=1.0;
//if ((p_hs->hs_number>=9)&&(p_hs->hs_number<=10)&&(p_hs->p_parent_m->t_counter>500)&&(p_hs->p_parent_m->t_counter<1700))
	//Ca_factor = 1e5;*/


	f[i_temp] = k_on_factor * p_hs->p_bp->a_on_rate * Ca_factor * pow(10.0,-p_hs->p_parent_m->current_pCa)*
						(n_overlap - n_available) * (1.0 + (k_coop_factor * p_hs->p_bp->k_coop) * gsl_pow_int(n_available/n_overlap,coop_power)) -
				k_off_factor * p_hs->p_bp->a_off_rate * (n_available-n_bound) * 
					(1.0 + (k_coop_factor * p_hs->p_bp->k_coop)*gsl_pow_int((n_overlap-n_available)/n_overlap,coop_power));
			
p_hs->p_profiler[TIMER_DERIVS]->stop_timing();

//printf("Exiting derivs %i\n",p_hs->p_parent_m->condition_number);

	return GSL_SUCCESS;
}

void hs::check_int(int i)
{
	if ((i<0)|(i>=n_array_length))
	{
		printf("Check int problem\n");
		exit(1);
	}
}

double hs::return_n_bound(const double * y)
{
	// Given a vector, returns n_bound, the number of sites that are bound

	// Variables
	int i,j,k;
	double n_bound = 0;

	// Code
	for (i=0;i<p_bp->m_no_of_isoforms;i++)
	{
		for (j=0;j<p_bp->no_of_states;j++)
		{
			if (p_bp->state_attached[j])
			{
				for (k=p_bp->n_vector_indices[j][0];
						k<=p_bp->n_vector_indices[j][1];k++)
				{
					n_bound = n_bound + y[k];
				}
			}
		}
	} 

	return n_bound;
}

int jacobian(double t, const double y[], double *dfdy,
	double dfdt[], void *params)
{
	// End
	return GSL_SUCCESS;
}


// Debugging functions
void hs::dump_populations_to_file(int append)
{
	// Dumps the cb_populations to file

	// Variables
	int i,j;				// counters
	int m_counter;	
	int state_counter;

	char output_file_string[MAX_STRING_LENGTH];
	FILE * output_file;

	// Code

	// Deduce the file_string
	sprintf_s(output_file_string,MAX_STRING_LENGTH,
		"%s\\cb_distributions_%i.txt",
		p_bp->output_dir_string,
		p_parent_m->condition_number+1);

	// Open the file
	// if append is 0, write headers, else append
	if (!append)
	{
		fopen_s(&output_file,output_file_string,"w");
	}
	else
	{
		fopen_s(&output_file,output_file_string,"a");
	}

	// Check there is a file to write to
	if (output_file==NULL)
	{
		printf("Error in hs::dump_populations_to_file\n");
		printf("Output file\n%s\ncould not be opened\n",output_file_string);
		printf("Now exiting\n");
		exit(1);
	}

	if (!append)
	{
		// Write headers
		fprintf(output_file,"no_of_half_sarcomeres: %i\n",p_bp->no_of_half_sarcomeres);
		fprintf(output_file,"no_of_m_isoforms: %i\n",p_bp->m_no_of_isoforms);
		fprintf(output_file,"no_of_attached_states: %i\n",p_bp->no_of_attached_states);
		fprintf(output_file,"Attached_states:");
		for (i=0;i<p_bp->no_of_states;i++)
		{
			if (p_bp->state_attached[i])
				fprintf(output_file,"\t%i",i+1);
		}
		fprintf(output_file,"\n");
		fprintf(output_file,"T_inc_s\t");
		for (m_counter=0;m_counter<p_bp->m_no_of_isoforms;m_counter++)
		{
			for (i=0;i<p_bp->no_of_attached_states;i++)
			{
				for (j=0;j<no_of_x_bins;j++)
				{
					fprintf(output_file,"%g",gsl_vector_get(x,j));
					if ((m_counter==(p_bp->m_no_of_isoforms-1))&&
							(i==(p_bp->no_of_attached_states-1))&&(j==(no_of_x_bins-1)))
						fprintf(output_file,"\n");
					else
						fprintf(output_file,"\t");
				}
			}
		}
	}

	// Dump populations
	fprintf(output_file,"%g\t",gsl_vector_get(p_bp->time_steps,p_parent_m->t_counter));
	state_counter=0;
	for (m_counter=0;m_counter<p_bp->m_no_of_isoforms;m_counter++)
	{
		for (i=0;i<p_bp->no_of_states;i++)
		{
			if (p_bp->state_attached[i])
			{
				state_counter++;

				for (j=p_bp->n_vector_indices[i][0];
					j<=p_bp->n_vector_indices[i][1];j++)
				{
					fprintf(output_file,"%g",gsl_matrix_get(n_matrix,j,m_counter));
			
					if ((m_counter==(p_bp->m_no_of_isoforms-1))&&
							(state_counter==p_bp->no_of_attached_states)&&
								(j==p_bp->n_vector_indices[i][1]))
						fprintf(output_file,"\n");
					else
						fprintf(output_file,"\t");
				}
			}
		}
	}

	// Tidy up
	fclose(output_file);
}

double hs::sum_of_n_matrix(void)
{
	// Code returns 1 if every element of the n_matrix is finite, zero otherwise

	// Variables
	int i;
	int j;

	int status=0;

	double n_value;
	double holder;

	// Code
	holder=0.0;

	for (j=0;j<p_bp->m_no_of_isoforms;j++)
	{
		for (i=0;i<n_array_length;i++)
		{
			n_value = gsl_matrix_get(n_matrix,i,j);

			holder=holder+n_value;
		}
	}

	return holder;
}

void hs::dump_status_to_file(int append)
{
	// Dumps the hs status to file

	// Variables
	int i,m_counter;
	FILE * output_file;
	char output_file_string[MAX_STRING_LENGTH];

	// Deduce the file_string
	if (strlen(p_bp->output_dir_string)>0)
	{
		sprintf_s(output_file_string,"%s\\%s_%i_%i.txt",
			p_bp->output_dir_string,
			HS_STATUS_FILE_STRING,
			p_parent_m->condition_number,
			hs_number);
	}
	else
	{
		sprintf_s(output_file_string,"%s_%i.txt",
			HS_STATUS_FILE_STRING,
			p_parent_m->condition_number);
	}

	// Open the file
	// if append is 0, write headers, else append
	if (!append)
		output_file=fopen(output_file_string,"w");
	else
		output_file=fopen(output_file_string,"a");

	// Check there is a file to write to
	if (output_file==NULL)
	{
		printf("Error in hs::dump_status_to_file\n");
		printf("Output file\n%s\ncould not be opened\n",output_file_string);
		printf("Now exiting\n");
		exit(1);
	}
	
	if (!append)
	{
		// Write headers
		fprintf(output_file,"m_no_of_isoforms: %i\n",p_bp->m_no_of_isoforms);
		fprintf(output_file,"Time(s)\thsl(nm)\tForce\t");
		for (m_counter=0;m_counter<p_bp->m_no_of_isoforms;m_counter++)
		{
			for (i=0;i<p_bp->no_of_states;i++)
				fprintf(output_file,"My%iS%i_force\t",m_counter,i+1);
		}
		fprintf(output_file,"CB_force\tPas_force\tViscous_force\t");
		for (m_counter=0;m_counter<p_bp->m_no_of_isoforms;m_counter++)
		{
			for (i=0;i<p_bp->no_of_states;i++)
				fprintf(output_file,"My%iS%i_pop\t",m_counter,i+1);
		}
		fprintf(output_file,"N_pop\talpha\tbeta\tCommand\tSlack\t");
		fprintf(output_file,"f_act\tf_overlap\tf_bound\n");
	}

	if (append)
	{
		// Write data
		fprintf(output_file,"%g\t%g\t%g\t",
			cumulative_time,
			hsl,
			actual_forces.total_force);
		for (m_counter=0;m_counter<p_bp->m_no_of_isoforms;m_counter++)
		{
			for (i=0;i<p_bp->no_of_states;i++)
				fprintf(output_file,"%g\t",gsl_matrix_get(cb_state_forces,i,m_counter));
		}
		fprintf(output_file,"%g\t%g\t%g\t",
			actual_forces.cb_force,
			actual_forces.passive_force,
			actual_forces.viscous_force);
		for (m_counter=0;m_counter<p_bp->m_no_of_isoforms;m_counter++)
		{
			for (i=0;i<p_bp->no_of_states;i++)
				fprintf(output_file,"%g\t",gsl_matrix_get(cb_state_pops,i,m_counter));
		}
		fprintf(output_file,"%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\n",
			N,
			alpha_value,
			beta_value,
			command_length,
			slack_length,
			f_activated,
			f_overlap,
			f_bound);
	}

	fclose(output_file);
}


void hs::dump_f_overlap_function_to_file(void)
{
	// Code dumps f_overlap

	// Variables
	double old_hsl;
	double min_hsl=750;
	double max_hsl=2000;
	double hsl_inc=1.0;

	char f_overlap_file_string[MAX_STRING_LENGTH];
	FILE *out_file;

	// Code

	// Deduce the file_string
	sprintf_s(f_overlap_file_string,MAX_STRING_LENGTH,
			"%s\\f_overlap.txt",
			p_bp->output_dir_string);

	// Open the file
	fopen_s(&out_file,f_overlap_file_string,"w");

	if (out_file==NULL)
	{
		printf("hs::dump_f_overlap_function_to_file()\nOutput file: %s could not be opened\n",
			f_overlap_file_string);
		printf("Now exiting\n");
		exit(1);
	}

	// Print the header
	fprintf(out_file,"hsl_nm\tf_overlap\n");

	// Store the current hsl
	// Scan it through a range
	// Reset the hsl

	old_hsl = hsl;

	hsl = min_hsl;
	while (hsl<max_hsl)
	{
		fprintf(out_file,"%g\t%g\n",hsl,return_f_overlap(hsl));

		hsl=hsl+hsl_inc;
	}

	// Tidy up
	hsl = old_hsl;
	//set_f_overlap();
	f_overlap = return_f_overlap(hsl);

	// Close file
	fclose(out_file);
}

void hs::dump_force_dependence(void)
{
	double f;
	char force_dependence_file_string[MAX_STRING_LENGTH];
	FILE *out_file;

	// Deduce the file_string
	sprintf_s(force_dependence_file_string,MAX_STRING_LENGTH,
		"%s\\force_dependence.txt",
		p_bp->output_dir_string);

	// Open the file
	fopen_s(&out_file,force_dependence_file_string,"w");

	if (out_file==NULL)
	{
		printf("hs::dump_force_dependence()\nOutput file: %s could not be opened\n",
			force_dependence_file_string);
		printf("Now exiting\n");
		exit(1);
	}

	fprintf(out_file,"f\tforce_dependence\n");

	for (f=0.0;f<1e5;f=f+1000)
	{
		fprintf(out_file,"%g\t%g\n",
			f,
			1.0 + 
				gsl_matrix_get(p_bp->fd_coefs,0,0)*
					(1-exp(-gsl_matrix_get(p_bp->fd_coefs,0,1)*f)));
	}

	fclose(out_file);

}

void hs::dump_rate_constants(void)
{
	// Code dumps rate constants

	// Variables
	int i,j,k;
	char rate_constants_file_string[MAX_STRING_LENGTH];
	FILE *out_file;

	// Deduce the file_string
	sprintf_s(rate_constants_file_string,MAX_STRING_LENGTH,
		"%s\\rates.txt",
		p_bp->output_dir_string);

	// Open the file
	fopen_s(&out_file,rate_constants_file_string,"w");

	if (out_file==NULL)
	{
		printf("hs::dump_rate_constants()\nOutput file: %s could not be opened\n",
			rate_constants_file_string);
		printf("Now exiting\n");
		exit(1);
	}

	// Print the header
	fprintf(out_file,"x(nm)");
	for (j=0;j<p_bp->m_no_of_isoforms;j++)
	{
		for (k=0;k<p_bp->no_of_transitions;k++)
		{
			fprintf(out_file,"\tM%i_Rate%i",(j+1),(k+1));
			
			if (k==(p_bp->no_of_transitions-1))
				fprintf(out_file,"\n");
		}
	}

	// Loop through x values
	for (i=0;i<p_bp->no_of_x_bins;i++)
	{
		fprintf(out_file,"%g",gsl_vector_get(x,i));
		// Loop through the isoforms
		for (j=0;j<p_bp->m_no_of_isoforms;j++)
		{
			// And the transitions
			for (k=0;k<p_bp->no_of_transitions;k++)
			{
				fprintf(out_file,"\t%g",
					generic_rate(k+1,gsl_vector_get(x,i),j));
				
				if (k==(p_bp->no_of_transitions-1))
					fprintf(out_file,"\n");
			}
			
		}
	}

	// Tidy up
	fclose(out_file);
}

void hs::dump_energies(void)
{
	// Code dumps cb energies

	// Variables
	int i,j,k;

	char output_file_string[MAX_STRING_LENGTH];

	gsl_matrix * energy_matrix;

	// Code
	energy_matrix = gsl_matrix_alloc(p_bp->no_of_x_bins,
		(p_bp->m_no_of_isoforms*p_bp->no_of_states)+1);

	// Loop
	for (i=0;i<p_bp->no_of_x_bins;i++)
	{
		// Set the x value
		gsl_matrix_set(energy_matrix,i,0,gsl_vector_get(x,i));

		// Loop through the isoforms
		for (j=0;j<p_bp->m_no_of_isoforms;j++)
		{
			// And the transitions
			for (k=0;k<p_bp->no_of_states;k++)
			{
				gsl_matrix_set(energy_matrix,i,(j*p_bp->no_of_states)+k+1,
					return_cb_energy(k,gsl_vector_get(x,i),j)/p_bp->beta);
			}
		}
	}

	// Dump
	
	// Deduce the file_string
	sprintf_s(output_file_string,MAX_STRING_LENGTH,
		"%s\\energies.txt",
		p_bp->output_dir_string);

	// Dump the matrix
	dump_gsl_matrix(energy_matrix,output_file_string);

	// Tidy up
	gsl_matrix_free(energy_matrix);
}


double hs::sum_of_gsl_vector(gsl_vector * gsl_v)
{
	// Code calculates the sum of a gsl_vector

	// Variables
	int i;
	double s;

	// Code
	s=0.0;
	for (i=0;i<(int)(gsl_v->size);i++)
		s=s+gsl_vector_get(gsl_v,i);

	// Return
	return s;
}


/*void hs::build_r_matrix(int m_isoform)
{
	// Code builds r_matrix

	// Variables
	int i,j;
	int a,b;

	int rate_index;
	int other_state;

	int bin_index;

	double rate;

	// Code
/*
	// First zero the matrix
	gsl_matrix_set_zero(r_matrix[m_isoform]);

	for (i=0;i<p_bp->no_of_states;i++)
	{
		if (p_bp->state_attached[i]==0)
		{
			// It's a detached state, decrements build on a single point
			// increments lie on a column
			a = p_bp->n_vector_indices[i][0];

			for (j=0;j<MAX_PATHWAYS;j++)
			{
				other_state = p_bp->state_transitions[i][j];

				if (other_state!=0)
				{
					// Set the indices for the rate function and the other state
					rate_index = p_bp->state_rate_functions[i][j];

					bin_index = 0;
					for (b=p_bp->n_vector_indices[other_state-1][0];
							p<=p_bp->n_vector_indices[other_state-1][1];b++)
					{
						rate = generic_rate(rate_index
					





/*
	// First, zero the matrix
	gsl_matrix_set_zero(r_matrix[m_isoform]);
	gsl_matrix_set_zero(a_matrix[m_isoform]);

	for (i=0;i<p_bp->no_of_states;i++)
	{
		if (p_bp->state_attached[i]==0)
		{
			// It's a detached state, decrements build on a single point,
			// increments lie on a column
			a = p_bp->n_vector_indices[i][0];

			for (j=0;j<MAX_PATHWAYS;j++)
			{
				if (p_bp->state_transitions[i][j]!=0)
				{
					// Set the indices for the rate function and the other state
					other_state=p_bp->state_transitions[i][j];
					rate_index=p_bp->state_rate_functions[i][j];

					bin_index=0;
					for (b=p_bp->n_vector_indices[other_state-1][0];
							b<=p_bp->n_vector_indices[other_state-1][1];b++)
					{
						rate=generic_rate(rate_index,gsl_vector_get(x,bin_index),m_isoform);

						// Correct for the width of the bin if it is an attachment step
						// Also update the a_matrix
						if (p_bp->state_attached[other_state-1])
						{
							rate = rate * p_bp->x_bin_increment;
							gsl_matrix_set(a_matrix[m_isoform],b,a,1.0);
						}

						// This is the decrement part of the transition
						gsl_matrix_set(r_matrix[m_isoform],a,a,
							gsl_matrix_get(r_matrix[m_isoform],a,a)-rate);

						// This is the increment part
						gsl_matrix_set(r_matrix[m_isoform],b,a,
							gsl_matrix_get(r_matrix[m_isoform],b,a)+rate);

						// Increment the bin index
						bin_index++;
					}
				}
			}
		}
		else
		{
			// There are multiple lines for the state in the r_matrix
			for (j=0;j<MAX_PATHWAYS;j++)
			{
				if (p_bp->state_transitions[i][j]!=0)
				{
					// Set the indices for the rate function and the other state
					other_state=p_bp->state_transitions[i][j];
					rate_index=p_bp->state_rate_functions[i][j];

					bin_index=0;
					for (a=p_bp->n_vector_indices[i][0];
						a<=p_bp->n_vector_indices[i][1];a++)
					{
						rate=generic_rate(rate_index,gsl_vector_get(x,bin_index),m_isoform);

						// This is the decrement
						gsl_matrix_set(r_matrix[m_isoform],a,a,
							gsl_matrix_get(r_matrix[m_isoform],a,a)-rate);

						// Now the increments
						// Are they going to a detached state?
						if (p_bp->state_attached[other_state-1]==0)
						{
							// Yes, so all increments go into a single row
							b=p_bp->n_vector_indices[other_state-1][0];
						}
						else
						{	// No, so increments go into different rows
							b=p_bp->n_vector_indices[other_state-1][0]+bin_index;
						}

						gsl_matrix_set(r_matrix[m_isoform],b,a,
							gsl_matrix_get(r_matrix[m_isoform],b,a)+rate);

						// Increment the bin_counter
						bin_index++;
					}
				}
			}
		}
	}
}
*/