// profiler.cpp

#include "stdafx.h"
#include <stdlib.h>
#include <math.h>

#include "hr_timer.h"

#include "profiler.h"

// Constructor
profiler::profiler()
{
	function_calls=0;
	timer=0;
}

profiler::~profiler()
{
}

void profiler::start_timing(void)
{
	function_calls++;
	stopwatch.startTimer();
}

void profiler::start_timing(int update)
{
	function_calls=function_calls+update;
	stopwatch.startTimer();
}

void profiler::stop_timing(void)
{
	stopwatch.stopTimer();
	timer=timer + stopwatch.getElapsedTime();
}





