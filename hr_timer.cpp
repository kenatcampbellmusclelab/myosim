#include "stdafx.h"
#include <windows.h>

#ifndef hr_timer
#include "hr_timer.h"
#define hr_timer
#endif

double CStopWatch::LIToSecs( LARGE_INTEGER & L) {
    return ((double)L.QuadPart /(double)frequency.QuadPart) ;
}

CStopWatch::CStopWatch(){
    timer.start.QuadPart=0;
    timer.stop.QuadPart=0;
	// Added by Ken
	no_of_calls=0;
	// End Ken
    QueryPerformanceFrequency( &frequency ) ;
}

void CStopWatch::startTimer( ) {
	// Added by Ken
	no_of_calls++;
	// End Ken
    QueryPerformanceCounter(&timer.start) ;
}

void CStopWatch::stopTimer( ) {
    QueryPerformanceCounter(&timer.stop) ;
}

double CStopWatch::getElapsedTime() {
    LARGE_INTEGER time;
    time.QuadPart = timer.stop.QuadPart - timer.start.QuadPart;
    return LIToSecs( time) ;
}
