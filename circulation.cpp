// Circulation.cpp
#include <stdafx.h>
#include <stdio.h>
#include <atlstr.h>
#include <math.h>

#include "base_parameters.h"
#include "global_definitions.h"
#include "circulation.h"
#include "muscle.h"
#include "hs.h"

#include "gsl/gsl_vector.h"
#include "gsl/gsl_matrix.h"
#include "gsl/gsl_rng.h"
#include "gsl/gsl_math.h"

// Function declarations for non-class members used for ODEs
int circ_derivs(double t, const double y[], double f[], void *params);

struct circ_params
{
	circulation * p_c;
};

// Constructor
circulation::circulation(base_parameters * set_p_bp, int set_condition_number, int set_thread_number)
{
	// Initialisation
	p_bp = set_p_bp;
	condition_number = set_condition_number;
	thread_number = set_thread_number;

	// Variables
	int i;

	// Code
	p_m = new muscle(p_bp,condition_number,thread_number);

	p_m->current_pCa = 9.0;
	p_m->isotonic_force = -2.0;
	p_m->step_simulation(0.001,0.0);

	// Deduce the slack hsl
	circulation_slack_hsl = p_m->p_hs[0]->return_hs_length_for_force(1.0,0.0001);

	// Set the muscle to that length
	p_m->step_simulation(0.0,circulation_slack_hsl - p_m->current_length);

	// Deduce the slack circumference of the ventricle
	slack_lv_circumference = 
		return_ventricular_circumference(p_bp->slack_ventricular_volume);

	// Set that circumference
	current_lv_circumference = slack_lv_circumference;

	// Set the volume
	lv_volume = p_bp->slack_ventricular_volume;

	// Initialise the resistances and the compliances
	r_values = gsl_vector_alloc(p_bp->circulation_no_of_compartments);
	c_values = gsl_vector_alloc(p_bp->circulation_no_of_compartments);
	gsl_vector_set_zero(r_values);
	gsl_vector_set_zero(c_values);

	for (i=0;i<p_bp->circulation_no_of_compartments;i++)
	{
		gsl_vector_set(r_values,i,
			gsl_vector_get(p_bp->circulation_base_r_values,i));
		gsl_vector_set(c_values,i,
			gsl_vector_get(p_bp->circulation_base_c_values,i));
	}

	// Initialise the compartment_volumes and pressures
	compartment_volumes = gsl_vector_alloc(p_bp->circulation_no_of_compartments);
	compartment_pressures = gsl_vector_alloc(p_bp->circulation_no_of_compartments);

	gsl_vector_set_zero(compartment_volumes);
	gsl_vector_set_zero(compartment_pressures);

	gsl_vector_set(compartment_volumes,p_bp->circulation_no_of_compartments-2,
		p_bp->circulation_total_volume - p_bp->slack_ventricular_volume);
	gsl_vector_set(compartment_volumes,p_bp->circulation_no_of_compartments-1,
		p_bp->slack_ventricular_volume);

	gsl_vector_set(compartment_pressures,p_bp->circulation_no_of_compartments-2,
		gsl_vector_get(compartment_volumes,p_bp->circulation_no_of_compartments-2)/
			gsl_vector_get(c_values,p_bp->circulation_no_of_compartments-2));

	/*
for (i=0;i<p_bp->circulation_no_of_compartments;i++)
{
	printf("c[%i] %g r[%i] %g\n",
		i,gsl_vector_get(r_values,i),i,gsl_vector_get(c_values,i));
}
*/



/*
	int i;
	double temp = 0.0;
	for (i=0;i<30;i++)
	{
		if (i>0)
			lv_volume = lv_volume + 10.0;

		printf("Vol: %g pressure: %g\n",
			lv_volume,return_ventricular_pressure(lv_volume));
	}
	*/
}

// Destructor
circulation::~circulation(void)
{
	// Tidy up

	// Free vectors
	gsl_vector_free(compartment_volumes);
	gsl_vector_free(compartment_pressures);

	gsl_vector_free(r_values);
	gsl_vector_free(c_values);

	delete p_m;
}

double circulation::return_ventricular_pressure(double lv_volume)
{
	// Returns ventricular pressure

	// Variables
	double new_lv_pressure;
	double new_lv_circumference;
	double delta_hsl;
	double muscle_force;
	
	// Code

	// Deduce the new circumference
	new_lv_circumference = return_ventricular_circumference(lv_volume);

	// Work out relative change in hsl
	delta_hsl = p_m->p_hs[0]->hsl * 
		((new_lv_circumference / current_lv_circumference)-1.0);

//printf("delta_hsl: %g\n",delta_hsl);

	// Deduce new force
	p_m->p_hs[0]->calculate_forces_and_pops(delta_hsl,0.0,0);

	// Turn that into a pressure
	new_lv_pressure = PASCALS_TO_MMHG_CONVERSION *
		p_m->p_hs[0]->estimated_forces.total_force *
		(-1.0 + pow((p_bp->myocardial_volume/lv_volume)+1.0,(2.0/3.0)));

	return new_lv_pressure;
}

double circulation::return_ventricular_circumference(double lv_volume)
{
	// Returns lv circumference for a given volume

	// Variables
	double circumference;

	// Code
	circumference = pow(6*gsl_pow_2(PI) *
		(lv_volume + (p_bp->myocardial_volume/2.0)),(1.0/3.0));

	return circumference;
}

int circ_derivs(double t, const double y[], double f[], void *params)
{
	// Returns the circulatory derivitives
	// volumes are passed in as a vector
	// Code knows about parent circulation object through a pointer
	// that is passed in through params

	// Variables
	int i;
	int n;
	
	struct circ_params *p =
		(struct circ_param *) params;

	circulation * p_c = p->p_c;

	// Code
	n = p_c->p_bp->circulation_no_of_compartments;

	// Allocate pressure array and fill it
	double pressures[n];

	for (i=0;i<(n-1);i++)
		pressures[i] = y[i] / p_c->c_values[i];

	pressures[n-1] = p_c->return_ventricular_pressure(p_c->lv_volume);

	// Zero volume derivitives
	for (i=0;i<n;i++)
		f[i]=0.0;

	// Now run through the compartments







}