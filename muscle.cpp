// muscle.cpp
#include <stdafx.h>

#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_multiroots.h>

#include "global_definitions.h"
#include "hs.h"
#include "base_parameters.h"
#include "gsl_fsolve_params.h"
#include "muscle.h"

// Constructor
muscle::muscle(base_parameters * set_p_bp, int set_condition_number, int set_thread_number)
{
	// Initialisation

	p_bp = set_p_bp;
	condition_number=set_condition_number;
	thread_number=set_thread_number;

	p_self_muscle = this;

	t_counter=0;

	isotonic_mode=1;
	isotonic_flag=0;

	p_fsolve_params = new gsl_fsolve_params();

	// Create arrays of alpha and beta values for initialization
	gsl_vector * alpha_values = gsl_vector_alloc(p_bp->no_of_half_sarcomeres);
	gsl_vector * beta_values = gsl_vector_alloc(p_bp->no_of_half_sarcomeres);

	double alpha_value;
	double alpha_holder;

	double beta_value;
	double beta_holder;

	int i;

	// Create n-1 random numbers centered around 1

	alpha_holder = 0.0;
	beta_holder = 0.0;

	for (i=0;i<((p_bp->no_of_half_sarcomeres)-1);i++)
	{
		alpha_value = 1.0 + 
			(gsl_rng_uniform(p_bp->rand_r)-0.5)*
			gsl_vector_get(p_bp->N_variability_vector,condition_number);

		alpha_holder = alpha_holder + alpha_value;

		gsl_vector_set(alpha_values,i,alpha_value);

printf("%i alpha_value %g  alpha_holder %g\n",
	i,alpha_value,alpha_holder);

		beta_value = 1.0 +
			(gsl_rng_uniform(p_bp->rand_r)-0.5)*
			gsl_vector_get(p_bp->P_variability_vector,condition_number);

		beta_holder = beta_holder + beta_value;

		gsl_vector_set(beta_values,i,beta_value);

printf("%i beta_value %g  beta_holder %g\n",
	i,beta_value,beta_holder);

	}
	// Make the last 1 so that the average is 1.0
	gsl_vector_set(alpha_values,(p_bp->no_of_half_sarcomeres-1),
		((double)p_bp->no_of_half_sarcomeres - alpha_holder));

	gsl_vector_set(beta_values,(p_bp->no_of_half_sarcomeres-1),
		((double)p_bp->no_of_half_sarcomeres - beta_holder));

// Check
for (i=0;i<(p_bp->no_of_half_sarcomeres);i++)
{
	printf("alpha[%i] %f\n",i,gsl_vector_get(alpha_values,i));
	printf("beta[%i] %f\n",i,gsl_vector_get(beta_values,i));
}

	if (p_bp->no_of_conditions>0)
	{
		// Initialise if there is data
		current_pCa = gsl_matrix_get(p_bp->pCa_values,0,condition_number);
		current_Pi_concentration = gsl_matrix_get(p_bp->Pi_concentrations,0,condition_number);
		current_ADP_concentration = gsl_matrix_get(p_bp->ADP_concentrations,0,condition_number);
		current_ATP_concentration = gsl_matrix_get(p_bp->ATP_concentrations,0,condition_number);
	}
	else
	{
		// Set some defaults
		current_pCa = 9.0;
		current_Pi_concentration = 0.0;
		current_ADP_concentration = 0.0;
		current_ATP_concentration = 0.0;
	}

	// Set series_values for the muscle
	if (gsl_vector_get(p_bp->series_k_vector,condition_number)>0.0)
		m_series_k = gsl_vector_get(p_bp->series_k_vector,condition_number);
	else
		m_series_k = p_bp->series_k;

	if (gsl_vector_get(p_bp->series_alpha_vector,condition_number)>0.0)
		m_series_alpha = gsl_vector_get(p_bp->series_alpha_vector,condition_number);
	else
		m_series_alpha = p_bp->series_alpha;

	if (gsl_vector_get(p_bp->series_beta_vector,condition_number)>0.0)
		m_series_beta = gsl_vector_get(p_bp->series_beta_vector,condition_number);
	else
		m_series_beta = p_bp->series_beta;

	// Adjust cooperativities if required
	if (gsl_vector_get(p_bp->k_minus_vector,condition_number)>0.0)
		m_k_minus = gsl_vector_get(p_bp->k_minus_vector,condition_number);
	else
		m_k_minus  = p_bp->k_minus;

	if (gsl_vector_get(p_bp->k_plus_vector,condition_number)>0.0)
		m_k_plus = gsl_vector_get(p_bp->k_plus_vector,condition_number);
	else
		m_k_plus  = p_bp->k_plus;
		
	// Create an array of half-sarcomeres
	for (i=0;i<p_bp->no_of_half_sarcomeres;i++)
	{
		if (p_bp->no_of_half_sarcomeres == 1)
		{
			p_hs[i] = new hs(i,p_bp->initial_hs_length+gsl_vector_get(p_bp->initial_dml,condition_number),
				p_bp,p_self_muscle,
				gsl_vector_get(alpha_values,i),gsl_vector_get(beta_values,i));
		}
		else
		{
			p_hs[i] = new hs(i,p_bp->initial_hs_length,p_bp,p_self_muscle,
				gsl_vector_get(alpha_values,i),gsl_vector_get(beta_values,i));
		}
	}

	if (p_bp->no_of_half_sarcomeres==1)
	{
		current_length = p_hs[0]->hsl;
		current_force = p_hs[0]->actual_forces.total_force;
	}
	else
	{
		current_length = 0.0;
		for (i=0;i<p_bp->no_of_half_sarcomeres_in_series;i++)
		{
			current_length = current_length + p_hs[i]->hsl;
		}
		current_force = p_hs[0]->actual_forces.total_force;
	}

	original_length = current_length;
	min_length = current_length;
	last_length = current_length;
	series_extension = 0.0;

	windkessel_force = 0;

	// Test
	if (p_bp->debug_dump_mode)
	{
		test_series_stretch();
	}

	// Tidy up
	gsl_vector_free(alpha_values);
	gsl_vector_free(beta_values);
}

// Destructor
muscle::~muscle(void)
{
	// Tidy up

	// Variables
	int i;

	// Code
	for (i=0;i<p_bp->no_of_half_sarcomeres;i++)
	{
		delete p_hs[i];
	}

	delete p_fsolve_params;
}

// Functions
void muscle::step_simulation(double time_step,double delta_hsl)
{
	// Steps the simulation

	// Variables
	double m_velocity;

	// Update the minimum length
	if (current_length<min_length)
		min_length = current_length;

	// Update isotonic force for Windkessel
	m_velocity = (current_length - last_length) / time_step;
	last_length = current_length;
	
	if (isotonic_flag > 0)
	{
		windkessel_force = windkessel_force - (p_bp->k_windkessel * m_velocity);

		if (windkessel_force > 0.0)
		{
			windkessel_force = windkessel_force - 
				time_step*(p_bp->d_windkessel * windkessel_force);
		}

		if (windkessel_force < 0.0)
		{
			windkessel_force = 0.0;
		}
	}
	else
	{
		windkessel_force = 0.0;
	}

	isotonic_force = isotonic_force + windkessel_force;
							
	// Check to see if we need to implement recovery mode
	if (p_bp->isotonic_on_mode>0)
	{
		if (gsl_vector_get(p_bp->ml_recovery_flag,condition_number)>0.5)
		{
			if (t_counter >= (int)gsl_vector_get(p_bp->ml_recovery_start,condition_number))
			{
				int i;
				double increment;

				increment = (original_length - current_length) /
								(gsl_vector_get(p_bp->ml_recovery_stop,condition_number) -
									gsl_vector_get(p_bp->ml_recovery_start,condition_number));


				for (i=gsl_vector_get(p_bp->ml_recovery_start,condition_number);
						i<=gsl_vector_get(p_bp->ml_recovery_stop,condition_number); i++)
				{
					gsl_matrix_set(p_bp->hs_increments,i,condition_number,
						gsl_matrix_get(p_bp->hs_increments,i,condition_number)+increment);

					gsl_matrix_set(p_bp->isotonic_forces,i,condition_number,-2.0);
				}

				gsl_vector_set(p_bp->ml_recovery_flag,condition_number,0.0);
			}
		}
	}

	// Code
	if ((p_bp->no_of_half_sarcomeres==1)&&(p_bp->series_compliance_effect<=0))
	{
		step_single_half_sarcomere(time_step,delta_hsl);
	}
	else
	{
		step_multiple_half_sarcomeres(time_step,delta_hsl);
	}

	if (p_bp->distributions_dump_mode==condition_number)
	{
		dump_populations_to_file();
	}
}

void muscle::step_single_half_sarcomere(double time_step,double delta_hsl)
{
	// Steps a single half-sarcomere

	// Variables
	double new_length;
	double adjustment;

	// Code

	// Update the command length
	p_hs[0]->command_length = p_hs[0]->command_length + delta_hsl;

	// Branch isotonic control
	if (p_bp->isotonic_on_mode<-0.5)
	{
		// Muscle is only ever in isometric mode
		isotonic_mode=0;
	}
	else
	{
		if (p_bp->isotonic_on_mode<0.5)
		{
			// Muscle is always in the default mode
			isotonic_mode=1;
		}
		else
		{
			// Start in isometric mode
			if (isotonic_flag==0)
				isotonic_mode=0;
			else
				isotonic_mode=1;

			if (isotonic_force>0.0)
			{
				if ((isotonic_flag==0)&&
						((current_force-isotonic_force)>0.0))
				{
					isotonic_mode=1;
					isotonic_flag=1;
				}

				// This section is to do with break out
				if (gsl_vector_get(p_bp->isotonic_off_mode,condition_number)>0.0)
				{
					// Break out when the muscle relengthens by a fixed amount
					if ((isotonic_flag==1) &&
						((current_length-min_length)>
							gsl_vector_get(p_bp->isotonic_off_mode,condition_number)))
					{
						isotonic_mode=0;
						isotonic_flag=0;
					}
				}
				else
				{
					// Break out when the muscle returns to its original length
					if ((isotonic_flag==1) &&
						(current_length>original_length))
					{
						isotonic_mode=0;
						isotonic_flag=0;
					}
				}
			}
		}
	}

	// Branch depending on the control mode
	if (isotonic_mode==0)
	{
		// This has to be length control mode
		p_hs[0]->slack_length = DEFAULT_HS_SLACK_LENGTH;
		p_hs[0]->implement_time_step(time_step,delta_hsl);
	}
	else
	{
		if (isotonic_force<=-1.5)
		{
			// This is the common length control mode
			p_hs[0]->slack_length = DEFAULT_HS_SLACK_LENGTH;
			p_hs[0]->implement_time_step(time_step,delta_hsl);
		}
		else
		{
			// Things are more complicated
			if (isotonic_force==ISOTONIC_ZERO_FORCE_CHECK)
			{
				// This means check that we aren't slack
				p_hs[0]->slack_length = p_hs[0]->return_hs_length_for_force(0.0,time_step);

				// The hs cannot be shorter than it's slack length
				new_length=GSL_MAX(p_hs[0]->slack_length,p_hs[0]->command_length);

				// The increment might be smaller than hsl if
				// we are catching up on slack
				adjustment = new_length - p_hs[0]->hsl;

				// Make the adjustment
				p_hs[0]->implement_time_step(time_step,adjustment);
			}
			else
			{
				// Do the root finding
				if (0)
				{
				new_length = p_hs[0]->return_hs_length_for_force(isotonic_force,time_step);
				adjustment = new_length - p_hs[0]->hsl;
				p_hs[0]->implement_time_step(time_step,adjustment);
				}
				else
				{
				// This means, that we are isotonic
				// One can get into trouble here if the kinetics are very fast
				// Thus do the turnover first, then move the bridges

				// Do the cb turnover
				p_hs[0]->implement_time_step(time_step,0.0,1,0);

				// Do the root finding
				new_length = p_hs[0]->return_hs_length_for_force(isotonic_force,time_step);
			
				// Deduce the length change
				adjustment = new_length - p_hs[0]->hsl;

				// Make the adjustment
				p_hs[0]->implement_time_step(time_step,adjustment,0,1);
				}
			}
		}
	}

	// Update muscle variables
	current_force = p_hs[0]->actual_forces.total_force;
	current_length = p_hs[0]->hsl;
}

void muscle::step_multiple_half_sarcomeres(double time_step,
	double delta_hsl)
{
	// Variables
	int i;

	// GSL
	const gsl_multiroot_fsolver_type *T;
	gsl_multiroot_fsolver *s;
	gsl_multiroot_function gsl_f;

	int gsl_status;
	size_t gsl_iter = 0;
	gsl_vector * gsl_x;
	gsl_vector * gsl_initial_hsl;

	// Update time step
	this->time_step = time_step;

	// Store initial hsl in case we need it later
	gsl_initial_hsl = gsl_vector_alloc(p_bp->no_of_half_sarcomeres);
	for (i=0;i<p_bp->no_of_half_sarcomeres;i++)
	{
		gsl_vector_set(gsl_initial_hsl,i,p_hs[i]->hsl);
	}

	// Evolve distributions in each half-sarcomere
	for (i=0;i<p_bp->no_of_half_sarcomeres;i++)
	{
		p_hs[i]->implement_time_step(time_step,delta_hsl,1);
		//p_hs[i]->implement_time_step(time_step,1.0,1);
	}

	// Set new muscle length
	for (i=0;i<p_bp->no_of_half_sarcomeres;i++)
	{
		p_hs[i]->command_length = p_hs[i]->command_length + delta_hsl;
	}

	// If we are in length control mode, set new current length
	if (isotonic_force < -1.5)
	{
		current_length = current_length +
			(double)p_bp->no_of_half_sarcomeres_in_series * delta_hsl;
	}

	// Prepare variables for force-balancing calculations
	gsl_x = gsl_vector_alloc(p_bp->no_of_half_sarcomeres);
	T = gsl_multiroot_fsolver_hybrids;
	s = gsl_multiroot_fsolver_alloc(T,p_bp->no_of_half_sarcomeres);

	// Set up a pointer to this muscle
	p_fsolve_params->p_muscle = this;
	p_fsolve_params->clamp_force = isotonic_force;

	// Work out what control modes we are in
	if (p_bp->isotonic_on_mode<-0.5)
	{
		// Muscle is only ever in isometric mode
		isotonic_mode=0;
	}
	else
	{
		// Are we holding a force clamp
		if (isotonic_flag==1)
			isotonic_mode=1;
		else
			isotonic_mode=0;

		if (isotonic_force>0)
		{
			// Do we need to start a force clamp
			if ((isotonic_flag==0)&&
				(current_force>isotonic_force))
			{
				isotonic_mode=1;
				isotonic_flag=1;
			}

			// Do we need to break out
			if (gsl_vector_get(p_bp->isotonic_off_mode,condition_number)>0)
			{
				// Break out when the muscle relengthens by a fixed amount
				if ((isotonic_flag==1) &&
					((current_length - min_length)>
						gsl_vector_get(p_bp->isotonic_off_mode,condition_number)))
				{
					isotonic_mode=0;
					isotonic_flag=0;
				}
			}
			else
			{
				// Break out when the muscle returns to its orginal length
				if ((isotonic_flag==1) &&
					(current_length>original_length))
				{
					isotonic_mode=0;
					isotonic_flag=0;
				}
			}
		}
	}

	// Branch depending on the control mode
	if ((isotonic_mode==0) || (isotonic_force < -1.5))
	{
		// We are in standard length control mode
		// Set the control mode in the gsl_params
		p_fsolve_params->length_control=1;

		// Initialise the guess
		initialize_gsl_length_control_vector(gsl_x,delta_hsl);
	}
	else
	{
		p_fsolve_params->length_control=0;
		initialize_gsl_tension_control_vector(gsl_x);

/*for (i=0;i<p_bp->no_of_half_sarcomeres;i++)
{
	printf("gsl_x[%i]: %g\n",i,gsl_vector_get(gsl_x,i));
}*/
	}

	// Common code
	gsl_f.f = &gsl_match_hs_forces_wrapper;
	gsl_f.n = p_bp->no_of_half_sarcomeres;
	gsl_f.params = p_fsolve_params;

	// Prepare
	gsl_multiroot_fsolver_set(s,&gsl_f,gsl_x);

	// Balance forces
	do
	{
		gsl_iter++;
		gsl_status = gsl_multiroot_fsolver_iterate(s);

		//print_gsl_fsolver_state(gsl_iter,s);

		if (gsl_status)
			break;

		gsl_status = gsl_multiroot_test_residual(s->f,FORCE_BALANCE_RESIDUAL);
	}
	while (gsl_status == GSL_CONTINUE && 
		gsl_iter < FORCE_BALANCE_MAX_ITERATIONS);

	// Tidy up if successful
	if (gsl_status == GSL_SUCCESS)
	{
		if (p_fsolve_params->length_control==1)
		{
			gsl_length_control_wrap_up(s->x);
		}
		else
		{
			gsl_tension_control_wrap_up(s->x);
		}
	}
	else
	{
		// Failed
		printf("muscle::step_multiple_half_sarcomeres\n");
		printf("Failed at step %i\n",t_counter);
		printf("Iterations: %i Max_iterations: %i\n",gsl_iter,FORCE_BALANCE_MAX_ITERATIONS);
		p_bp->error_flag=1;
		for (i=0;i<p_bp->no_of_half_sarcomeres;i++)
		{
			p_hs[i]->hsl = gsl_vector_get(gsl_initial_hsl,i);
		}
	}

	// Update hsl_last_velocity
	for (i=0;i<p_bp->no_of_half_sarcomeres;i++)
	{
		p_hs[i]->last_velocity = 
			(p_hs[i]->hsl - gsl_vector_get(gsl_initial_hsl,i))/time_step;
	}

	// Update muscle variables
	current_force = p_hs[0]->actual_forces.total_force;

	// Tidy up
	gsl_multiroot_fsolver_free(s);
	gsl_vector_free(gsl_x);
	gsl_vector_free(gsl_initial_hsl);
}

void muscle::initialize_gsl_length_control_vector(gsl_vector * x,
	double delta_hsl)
{
	// Code sets the first estimate of the solution vector

	// Variables
	int hs_index;
	int myofibril_counter;
	int serial_counter;

	// Code
	hs_index=0;

	// Cycle through the muscle system
	for (myofibril_counter=0;myofibril_counter<p_bp->no_of_myofibrils;
		myofibril_counter++)
	{
		for (serial_counter=0;serial_counter<p_bp->no_of_half_sarcomeres_in_series;
			serial_counter++)
		{
			if (serial_counter<(p_bp->no_of_half_sarcomeres_in_series-1))
			{
				gsl_vector_set(x,hs_index,
					p_hs[hs_index]->hsl + delta_hsl);
			}
			else
			{
				if (myofibril_counter>0)
				{
					// Problem
					printf("muscle::initialize_gsl_length_control_vector\n");
					printf("multiple myofibrils not yet implemented\n");
					printf("now exiting\n");
					exit(1);
				}
				else
				{
					gsl_vector_set(x,hs_index,current_force);
				}
			}

			hs_index++;
		}
	}
}

void muscle::initialize_gsl_tension_control_vector(gsl_vector * x)
{
	// Code sets the first estimate of the solution vector

	// Variables
	int i;

	for (i=0;i<p_bp->no_of_half_sarcomeres;i++)
	{
		gsl_vector_set(x,i,p_hs[i]->hsl);
	}
}

int muscle::gsl_match_hs_forces_wrapper(const gsl_vector *x, void *params,
	gsl_vector *f)
{
	// This function has to be static to work with GSL and therefore cannot
	// directly know about the specific muscle object it is being used with
	// It is called by the multiroot solvers and redirects the input
	// to a non-static function, which knows which muscle object to work with
	// because a pointer to the muscle is passed in inside the params structure

	// This should be thread-safe because different threads are passing addresses
	// to different params

	// Create a pointer to the muscle we want to work withx
	muscle * p_m_from_static = ((class gsl_fsolve_params *) params)->p_muscle;

	// Deduce whether we are doing length control or tension controlx

	int length_control_mode = 
		((class gsl_fsolve_params *) params)->length_control;

	if (length_control_mode==1)
		p_m_from_static->gsl_match_hs_forces_length_control(x,f);
	else
		p_m_from_static->gsl_match_hs_forces_tension_control(x,f);

	return GSL_SUCCESS;
}

void muscle::gsl_match_hs_forces_length_control(const gsl_vector * x, gsl_vector * f)
{
	// Variables
	int i;

	double holder;
	double stretch;
	double f_temp;

	gsl_vector * hsl = gsl_vector_alloc(p_bp->no_of_half_sarcomeres);
	gsl_vector * hsf = gsl_vector_alloc(p_bp->no_of_half_sarcomeres);

	// Code
	if (p_bp->no_of_myofibrils>1)
	{
		printf("muscle::gsl_match_hs_forces_length_control\n");
		printf("Not yet implemented for multiple_myofibrils\n");
		printf("Now exiting\n");
		exit(1);
	}

	// Set the length of each half_sarcomere
	holder=0.0;
	for (i=0;i<p_bp->no_of_half_sarcomeres_in_series;i++)
	{
		if (i<(p_bp->no_of_half_sarcomeres_in_series-1))
		{
			gsl_vector_set(hsl,i,gsl_vector_get(x,i));
			holder=holder+gsl_vector_get(x,i);
		}
		else
		{
			f_temp=gsl_vector_get(x,(p_bp->no_of_half_sarcomeres-1));
			stretch=return_series_stretch(f_temp);
			gsl_vector_set(hsl,i,current_length-stretch-holder);
		}
	}
	// Now calculate the hs forces
	for (i=0;i<p_bp->no_of_half_sarcomeres;i++)
	{
		p_hs[i]->calculate_forces_and_pops(
			(gsl_vector_get(hsl,i)-p_hs[i]->hsl),
			(gsl_vector_get(hsl,i)-p_hs[i]->hsl)/time_step,
			0);

		gsl_vector_set(hsf,i,p_hs[i]->estimated_forces.total_force);
	}

	// Now calculate the f array that should be minimized
	for (i=0;i<p_bp->no_of_half_sarcomeres;i++)
	{
		gsl_vector_set(f,i,
			gsl_vector_get(hsf,i)-gsl_vector_get(x,p_bp->no_of_half_sarcomeres-1));
	}

	// Tidy up
	gsl_vector_free(hsl);
	gsl_vector_free(hsf);
}

void muscle::gsl_match_hs_forces_tension_control(const gsl_vector * x, gsl_vector * f)
{
	// Variables
	int i;

	double holder;
	double stretch;
	double f_temp;

	gsl_vector * hsl = gsl_vector_alloc(p_bp->no_of_half_sarcomeres);
	gsl_vector * hsf = gsl_vector_alloc(p_bp->no_of_half_sarcomeres);

	// Code
	if (p_bp->no_of_myofibrils>1)
	{
		printf("muscle::gsl_match_hs_forces_tension_control\n");
		printf("not yet implemented for multiple myofibrils\n");
		printf("Now exiting\n");
		exit(1);
	}

	// Set the length of each half-sarcomere
	holder = 0.0;
	for (i=0;i<p_bp->no_of_half_sarcomeres_in_series;i++)
	{
		gsl_vector_set(hsl,i,gsl_vector_get(x,i));
	}

	// Now calculate the hs forces
	for (i=0;i<p_bp->no_of_half_sarcomeres_in_series;i++)
	{
		p_hs[i]->calculate_forces_and_pops(
			(gsl_vector_get(hsl,i)-p_hs[i]->hsl),
			(gsl_vector_get(hsl,i)-p_hs[i]->hsl)/time_step,
			0);

		gsl_vector_set(hsf,i,p_hs[i]->estimated_forces.total_force);
	}
	
	// Now calculate the f array that should be minimized
	for (i=0;i<p_bp->no_of_half_sarcomeres_in_series;i++)
	{
		gsl_vector_set(f,i,gsl_vector_get(hsf,i)-isotonic_force);
	}

	// Tidy up
	gsl_vector_free(hsl);
	gsl_vector_free(hsf);
}

void muscle::gsl_length_control_wrap_up(gsl_vector * x)
{
	// Code tidies up after a length control step with multiple half-sarcomeres

	// Variables
	int i;

	double holder;
	double adjustment;
	double stretch;
	double f_temp;

	// Code
	if (p_bp->no_of_myofibrils>1)
	{
		printf("muscle::gsl_length_control_wrap_up\n");
		printf("Not yet implemented for multiple_myofibrils\n");
		printf("Now exiting\n");
		exit(1);
	}

	holder = 0.0;
	for (i=0;i<p_bp->no_of_half_sarcomeres;i++)
	{
		if (i<(p_bp->no_of_half_sarcomeres-1))
		{
			adjustment = gsl_vector_get(x,i) - p_hs[i]->hsl;
			p_hs[i]->implement_time_step(time_step,adjustment,
				0,1);

			holder = holder + p_hs[i]->hsl;
		}
		else
		{
			f_temp=gsl_vector_get(x,(p_bp->no_of_half_sarcomeres-1));
			stretch=return_series_stretch(f_temp);

			adjustment = current_length - holder - stretch - p_hs[i]->hsl;

			p_hs[i]->implement_time_step(time_step,adjustment,0,1);
		}
	}

	series_extension = stretch;
}

void muscle::gsl_tension_control_wrap_up(gsl_vector * x)
{
	// Variables
	int i;

	double adjustment;

	// Code
	if (p_bp->no_of_myofibrils>1)
	{
		printf("muscle::gsl_tension_control_wrap_up\n");
		printf("Not yet implemented for multiple_myofibrils\n");
		printf("Now exiting\n");
		exit(1);
	}

	// Set muscle length
	current_length = 0.0;

	for (i=0;i<p_bp->no_of_half_sarcomeres_in_series;i++)
	{
		adjustment = gsl_vector_get(x,i) - p_hs[i]->hsl;
		p_hs[i]->implement_time_step(time_step,adjustment,0,1);

		// Adjust length
		current_length = current_length + gsl_vector_get(x,i);
	}

	series_extension = return_series_stretch(isotonic_force);

	current_length = current_length + series_extension;
}

double muscle::return_series_stretch(double f)
{
	// Returns stretch of series elastic element
	
	double x;
	double a,b,c;

	switch ((int)p_bp->series_compliance_effect)
	{
		case 0:
			// No series compliance
			x=0;
			break;

		case 1:
			// f=passive_f(x,s)
			// f=series_k * x;
			x = f / m_series_k;
			break;

		case 2:
			// f=passive_f(x,s)
			// f=beta*(exp(alpha*x)-1);
			if (f>0)
				x = (1.0/m_series_alpha) * log((f/m_series_beta) + 1.0);
			else
				x = -(1.0/m_series_alpha) * log(1.0 - (f/m_series_beta));
			break;

		case 3:
			// f = alpha / (beta-x+eps) - (alpha/beta)
			
			if (fabs(f)<ISOTONIC_FORCE_HSL_TOLERANCE)
				x = 0.0;
			else
				x = (f*m_series_beta) / ((m_series_alpha / m_series_beta) + f);

			//printf("m_series_alpha %g m_series_alpha %g\n",m_series_alpha,m_series_beta);
			//printf("f = %g x = %g\n",f,x);
			break;

		default:
			x=0;
	}

	return x;
}

void muscle::test_series_stretch(void)
{
	// Code tests series stretch
	FILE * output_file;
	char output_file_string[MAX_STRING_LENGTH];
	sprintf(output_file_string,
		"%s\\series_stretch.txt",p_bp->output_dir_string);
	output_file=fopen(output_file_string,"w");

	double f;
	double x;

	printf("starting test_series_stretch\n");
	for (f=-1e4;f<=1e5;f=f+1e3)
	{
		x=return_series_stretch(f);
		printf("x: %g\tf:%g\n",x,f);
		fprintf(output_file,"%g\t%g\n",x,f);
	}

	fclose(output_file);
}

void muscle::dump_populations_to_file(void)
{
	// Dumps the cb_distributions to file

	// Variables
	int i,j;
	int hs_counter;
	int m_counter;
	int state_counter;
	int bin_counter;

	char output_file_string[MAX_STRING_LENGTH];
	FILE * output_file;

	// Code

	// Deduce the the file string
	sprintf(output_file_string,"%s\\cb_distributions_%i.txt",
		p_bp->output_dir_string,
		condition_number+1);

	// Clean if required
	if (t_counter==1)
	{
		output_file = fopen(output_file_string,"w");
	}
	else
	{
		output_file = fopen(output_file_string,"a");
	}

	// Check there is a file to write to
	if (output_file==NULL)
	{
		printf("Error in hs::dump_populations_to_file\n");
		printf("Output file\n%s\ncould not be opened\n",output_file_string);
		printf("Now exiting\n");
		exit(1);
	}

	if (t_counter==1)
	{
		// Write the headers

		fprintf(output_file,"no_of_half_sarcomeres: %i\n",p_bp->no_of_half_sarcomeres);
		fprintf(output_file,"no_of_m_isoforms: %i\n",p_bp->m_no_of_isoforms);
		fprintf(output_file,"no_of_attached_states: %i\n",p_bp->no_of_attached_states);
		fprintf(output_file,"Attached_states:");
		for (i=0;i<p_bp->no_of_states;i++)
		{
			if (p_bp->state_attached[i])
				fprintf(output_file,"\t%i",i+1);
		}
		fprintf(output_file,"\n");
		fprintf(output_file,"T_inc_s\t");
		
		for (hs_counter=0;hs_counter<p_bp->no_of_half_sarcomeres;hs_counter++)
		{
			for (m_counter=0;m_counter<p_bp->m_no_of_isoforms;m_counter++)
			{
				for (state_counter=0;state_counter<p_bp->no_of_attached_states;state_counter++)
				{
					for (bin_counter=0;bin_counter<p_bp->no_of_x_bins;bin_counter++)
					{
						fprintf(output_file,"%g",
							gsl_vector_get(p_hs[0]->x,bin_counter));

						if ((hs_counter==(p_bp->no_of_half_sarcomeres-1)) &&
							(m_counter==(p_bp->m_no_of_isoforms-1)) &&
							(state_counter==(p_bp->no_of_attached_states-1)) &&
							(bin_counter==(p_bp->no_of_x_bins-1)))
						{
							fprintf(output_file,"\n");
						}
						else
						{
							fprintf(output_file,"\t");
						}
					}
				}
			}
		}
	}

	// Dump

	fprintf(output_file,"%g\t",
		gsl_vector_get(p_bp->time_steps,t_counter));

	for (hs_counter=0;hs_counter<p_bp->no_of_half_sarcomeres;hs_counter++)
	{
		state_counter=0;

		for (m_counter=0;m_counter<p_bp->m_no_of_isoforms;m_counter++)
		{
			for (i=0;i<p_bp->no_of_states;i++)
			{
				if (p_bp->state_attached[i])
				{
					state_counter++;

					for (j=p_bp->n_vector_indices[i][0];
						j<=p_bp->n_vector_indices[i][1];j++)
					{
						fprintf(output_file,"%g",
							gsl_matrix_get(p_hs[hs_counter]->n_matrix,j,m_counter));

						if ((hs_counter==(p_bp->no_of_half_sarcomeres-1)) &&
							(m_counter==(p_bp->m_no_of_isoforms-1)) &&
							(state_counter==p_bp->no_of_attached_states) &&
							(j==p_bp->n_vector_indices[i][1]))
						{
							fprintf(output_file,"\n");
						}
						else
						{
							fprintf(output_file,"\t");
						}
					}
				}
			}
		}
	}

	// Tidy up
	fclose(output_file);
}