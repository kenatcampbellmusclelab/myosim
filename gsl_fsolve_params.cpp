// gsl_fsolve_params.cpp

#include "stdafx.h"
#include "muscle.h"
#include "gsl_fsolve_params.h"

// Constructor
gsl_fsolve_params::gsl_fsolve_params()
{
	p_muscle = NULL;
	length_control = 1;
	clamp_force = 0;
}

// Destructor
gsl_fsolve_params::~gsl_fsolve_params()
{
}